/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.adminModel;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import patientmanagementsystem.doctorModel.Doctor;

public class AdminTest {
    
    private Admin testAdmin;
    private Doctor testDoc;
    
    public AdminTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        String first = "Sarah";
        String last = "Sar";
        String userId = "A1234";
        String password = "password";
        String address = "address";
        String postCode = "pl1 123";
        
        testAdmin = new Admin(first, last, userId, password, address, postCode);
        
        first = "Suzi";
        last = "Suz";
        userId = "D1234";
        password = "password";
        address = "another address";
        postCode = "PL4 123";
        
        testDoc = new Doctor(first, last, userId, password, address, postCode);
    
    }
    
    @After
    public void tearDown() {
        
    }

    @Test
    public void testGiveFeedback() {
        String rating = "Good";
        String comment ="Comment";
        
        System.out.println("Test giving feedback");
        
        Feedback feedback = new Feedback(testDoc, rating, comment );
        testDoc.receiveFeedback(feedback);
        
        ArrayList<Feedback> feedbacks = testDoc.getAllFeedback();
        
        assertEquals(feedbacks.get(0).getComment(), comment);
        assertEquals(feedbacks.get(0).getRating(), rating);
    }

}
