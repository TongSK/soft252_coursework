/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.secretaryModel;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import jdk.nashorn.internal.ir.RuntimeNode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.doctorModel.Prescription;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Awaiting;
import patientmanagementsystem.model.Medicine;
import patientmanagementsystem.patientModel.Patient;

public class SecretaryTest {
    
    private Secretary testSecretary;
    private ArrayList<RuntimeNode.Request> allRequests = new ArrayList<RuntimeNode.Request>();
    private Doctor testDoc;
    private Patient testPatient;
    private Appointment appointment;
    
    public SecretaryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        System.out.println("Setting up secretary");
        String first = "David";
        String last = "Dav";
        String userId = "S1234";
        String password = "password";
        String address = "address";
        String postCode = "PL1 123";
        
        testSecretary = new Secretary(first, last, userId, password, address, postCode);
        
        first = "Suzi";
        last = "Suz";
        userId = "D1234";
        password = "password";
        address = "another address";
        postCode = "PL4 123";
        
        testDoc = new Doctor(first, last, userId, password, address, postCode);
        
        first = "Billie";
        last = "Bill";
        userId = "P1235";
        password = "password";
        address = "an address";
        postCode = "PL9 123";
        String gender = "Female";
        LocalDate birthday = LocalDate.of(1999, Month.OCTOBER, 12);
        
        testPatient = new Patient(first, last, userId, password, address, postCode, gender, birthday);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCreateAppointment() {
        //Create appointment
        System.out.println("Testing appointment creation");
        String dateTime = "12/10/2019";
        appointment = new Appointment(testDoc, testPatient, dateTime);
        
        //Test if values are the same from creation
        assertEquals(dateTime, appointment.getDateTime());
        assertEquals(testDoc, appointment.getDoctor());
        assertEquals(testPatient, appointment.getPatient());
        
        ArrayList<Prescription> prescribedMedicine = new ArrayList<Prescription>();
        assertEquals(prescribedMedicine,appointment.getPrescribedMedicine());
        
        //Test if notes default empty
        String emptyNotes = "";
        assertEquals(emptyNotes, appointment.getConsultationNotes());
        
        //Test default IState object
        Awaiting awaiting = new Awaiting();
        assertEquals(awaiting.getClass(), appointment.getAppointmentStatus().getClass());
        
        //Test if default status
        String prescriptionStatus = "NO PRESCRIPTION";
        assertEquals(prescriptionStatus, appointment.getPrescriptionStatus());
    }

    @Test
    public void testSendAppointment() {
        System.out.println("Testing sending appointment");
        
        //Create an appointment
        String dateTime = "12/10/2019";
        appointment = new Appointment(testDoc, testPatient, dateTime);
        ArrayList<Appointment> appointments = testPatient.getAllAppointments();
        
        //Add to patient's appointment arrayList 
        appointments.add(appointment);
        testPatient.setAllAppointments(appointments);
        
        assertEquals(appointments.get(0), testPatient.getAllAppointments().get(0));
        
        //Test if date time is equal
        assertEquals(appointment.getDateTime(),testPatient.getAllAppointments().get(0).getDateTime());
        
        //Test if doctor is the same
        assertEquals(appointment.getDoctor().getFirstName(),testPatient.getAllAppointments().get(0).getDoctor().getFirstName());
        
        //Test if patient is the same
        assertEquals(appointment.getPatient().getFirstName(),testPatient.getAllAppointments().get(0).getPatient().getFirstName());
    }

    @Test
    public void testCreatePatientAccount() {
        System.out.println("Testing account creation");
        String first = "Lillie";
        String last = "Lill";
        String userId = "P1236";
        String password = "password";
        String address = "an address";
        String postCode = "PL3 123";
        String gender = "Female";
        LocalDate birthday = LocalDate.of(1997, Month.APRIL, 13);
        
        Patient patient = new Patient(first, last, userId, password, address, postCode, gender, birthday);
        assertEquals(first, patient.getFirstName());
        assertEquals(last, patient.getLastName());
        assertEquals(userId, patient.getUserId());
        assertEquals(password, patient.getPassword());
        assertEquals(address, patient.getAddress());
        assertEquals(postCode, patient.getPostCode());
        assertEquals(birthday, patient.getDateOfBirth());
        
    }

    @Test
    public void testGiveMedicine() {
        String name = "Medicine";
        Medicine testMedicine = new Medicine(name);
        assertEquals(name, testMedicine.getName());
        //Should be 0 by default
        assertEquals(0, testMedicine.getStock());
        
    }

    @Test
    public void testUpdateMedStock() {
        System.out.println("Testing update med stock");
        String name = "Medicine";
        Medicine testMedicine = new Medicine(name);
        
        int newStock = 15;
        testMedicine.setStock(newStock);
        assertEquals(15, testMedicine.getStock());
        
        name = "Another medicine";
        testMedicine = new Medicine(name);
        int anotherStock = 20;
        Medicine outputMed = testSecretary.updateMedStock(testMedicine, anotherStock);
        assertEquals(outputMed.getStock(), testMedicine.getStock());
        assertEquals(outputMed.getName(), testMedicine.getName());
    }
    
}
