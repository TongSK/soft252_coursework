/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.Controller;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import patientmanagementsystem.secretaryModel.Secretary;

public class PasswordControllerTest {
    private Secretary testSecretary;
    
    public PasswordControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        String first = "David";
        String last = "Dav";
        String userId = "S1234";
        String password = "password";
        String address = "address";
        String postCode = "PL1 123";
        
        testSecretary = new Secretary(first, last, userId, password, address, postCode);
        
    }

    @Test
    public void testChangePassword() {
        
        assertEquals(testSecretary.getPassword(), "password");
        
        String newPassword = "newPassword";
        
        testSecretary.setPassword(newPassword);
        
        assertEquals(testSecretary.getPassword(), newPassword);
        
        assertNotEquals(testSecretary.getPassword(), "password");
    }
    
}
