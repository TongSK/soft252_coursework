/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.doctorModel;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import patientmanagementsystem.adminModel.Admin;
import patientmanagementsystem.adminModel.Feedback;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Awaiting;
import patientmanagementsystem.model.Consulted;
import patientmanagementsystem.model.Medicine;
import patientmanagementsystem.patientModel.AppointmentRequest;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.secretaryModel.Secretary;

public class DoctorTest {
    
    private Secretary testSecretary;
    private Doctor testDoc;
    private Patient testPatient;
    private Admin testAdmin;
    
    public DoctorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        String first = "David";
        String last = "Dav";
        String userId = "S1234";
        String password = "password";
        String address = "address";
        String postCode = "PL1 123";
        
        testSecretary = new Secretary(first, last, userId, password, address, postCode);
        
        first = "Suzi";
        last = "Suz";
        userId = "D1234";
        password = "password";
        address = "another address";
        postCode = "PL4 123";
        
        testDoc = new Doctor(first, last, userId, password, address, postCode);
        
        first = "Billie";
        last = "Bill";
        userId = "P1235";
        password = "password";
        address = "an address";
        postCode = "PL9 123";
        String gender = "Female";
        LocalDate birthday = LocalDate.of(1999, Month.OCTOBER, 12);
        
        testPatient = new Patient(first, last, userId, password, address, postCode, gender, birthday);
        
        first = "Sarah";
        last = "Sar";
        userId = "A1234";
        password = "password";
        address = "address";
        postCode = "pl1 123";
        
        testAdmin = new Admin(first, last, userId, password, address, postCode);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testReceiveFeedback() {
        String rating = "Good";
        String comment ="Comment";
        
        System.out.println("Test receiving feedback");
        
        Feedback feedback = new Feedback(testDoc, rating, comment );
        
        testAdmin.giveFeedback(testDoc, feedback);
        
        assertEquals(testDoc.getAllFeedback().get(0).getComment(), comment);
        
    }

    @Test
    public void testCreateMedicine() {
        String name = "medicine name";
        Medicine checkMed = testDoc.createMedicine(name, testSecretary);
        
        assertEquals(checkMed.getName(), name);
        assertNotNull(testSecretary.getAllNotifications().get(0));
        
        Medicine med = new Medicine("Test request type");
        OrderRequest requestMed = new OrderRequest(med);
        //Check if an order request was made after medicine was created
        assertEquals(testSecretary.getAllRequests().get(0).getRequestType(), requestMed.getRequestType() );
    }

    @Test
    public void testCreatePrescription() {
        
        System.out.println("Test creating prescription");
        String date = "10/10/10";
        Appointment appointment = new Appointment(testDoc, testPatient, date);
        
        testSecretary.sendAppointment(testPatient, appointment);
        
         String name = "medicine name";
         Medicine medicine = new Medicine(name);
         
         int quantity = 3;
         String dosage = "JDNJSJDN";
         
         testDoc.createPrescription(appointment, medicine, quantity, dosage);
         
         //Check if prescription added is set to default false
         assertFalse(testPatient.getAllAppointments().get(0).getPrescribedMedicine().get(0).getReceived());
         
         assertEquals(testPatient.getAllAppointments().get(0).getPrescribedMedicine().get(0).getQuantity(), quantity);
         
         assertEquals(testPatient.getAllAppointments().get(0).getPrescribedMedicine().get(0).getDosage(), dosage);
         
         assertEquals(testPatient.getAllAppointments().get(0).getPrescribedMedicine().get(0).getMedicine().getName(), name);
    }

    @Test
    public void testFinishAppointment() {
        System.out.println("Test finish appointment");
        String date = "10/10/10";
        Appointment appointment = new Appointment(testDoc, testPatient, date);
        
        testSecretary.sendAppointment(testPatient, appointment);
        
        Awaiting awaiting = new Awaiting();
        assertEquals(appointment.getAppointmentStatus().getClass(), awaiting.getClass());
        
        String notes = "hjsdhfhdsjhdfjf";
        testDoc.finishAppointment(appointment, notes);
        
        //Check if appointment changed to new state
        Consulted finish = new Consulted();
        assertEquals(appointment.getAppointmentStatus().getClass(), finish.getClass());
        
        assertEquals(appointment.getConsultationNotes(), notes);
    }

    @Test
    public void testSetResponse() {
        
        System.out.println("Test Doctor reponse");
        
        String date = "10/10/10";
        String thirdDate = "12/12/12";
        ArrayList<String> dates = new ArrayList<String>();
        dates.add(date);
        dates.add(date);
        dates.add(thirdDate);
        
        AppointmentRequest request = new AppointmentRequest(testDoc, testPatient, dates, testSecretary);
        
        String response = "20/2/20";
        
        testDoc.setResponse(request, response, testSecretary);
        
        assertEquals(request.getProposedDate(), response);
        assertEquals(request.getRequestStatus(), "DOCTOR PROPOSAL");
        
        //Check if secretary receives same  request
        assertEquals(request.getRequestType(), testSecretary.getAllRequests().get(0).getRequestType());
        
    }

    
}
