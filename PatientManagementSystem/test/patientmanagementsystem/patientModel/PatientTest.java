/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.patientModel;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import jdk.nashorn.internal.ir.RuntimeNode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import patientmanagementsystem.adminModel.Admin;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.secretaryModel.Secretary;

public class PatientTest {
    
    private Secretary testSecretary;
    private Doctor testDoc;
    private Patient testPatient;
    private Admin testAdmin;
    
    public PatientTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        String first = "David";
        String last = "Dav";
        String userId = "S1234";
        String password = "password";
        String address = "address";
        String postCode = "PL1 123";
        
        testSecretary = new Secretary(first, last, userId, password, address, postCode);
        
        first = "Suzi";
        last = "Suz";
        userId = "D1234";
        password = "password";
        address = "another address";
        postCode = "PL4 123";
        
        testDoc = new Doctor(first, last, userId, password, address, postCode);
        
        first = "Billie";
        last = "Bill";
        userId = "P1235";
        password = "password";
        address = "an address";
        postCode = "PL9 123";
        String gender = "Female";
        LocalDate birthday = LocalDate.of(1999, Month.OCTOBER, 12);
        
        testPatient = new Patient(first, last, userId, password, address, postCode, gender, birthday);
        
        first = "Sarah";
        last = "Sar";
        userId = "A1234";
        password = "password";
        address = "address";
        postCode = "pl1 123";
        
        testAdmin = new Admin(first, last, userId, password, address, postCode);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testReceiveAppointment() {
        System.out.println("Test receive appointment");
        String date = "10/10/10";
        Appointment appointment = new Appointment(testDoc, testPatient, date);
        
        testSecretary.sendAppointment(testPatient, appointment);
        
        ArrayList<Appointment> allAppointments = testPatient.getAllAppointments();
        
        //Check date
        assertEquals(allAppointments.get(0).getDateTime(), date);
        
        //Check doctors name
        assertEquals(allAppointments.get(0).getDoctor().getFirstName(), appointment.getDoctor().getFirstName());
        
        //Check doctors name
        assertEquals(allAppointments.get(0).getPatient().getFirstName(), appointment.getPatient().getFirstName());
    }


    @Test
    public void testSetResponse() {
        System.out.println("Test set response");
        String date = "10/10/10";
        String thirdDate = "12/12/12";
        ArrayList<String> dates = new ArrayList<String>();
        dates.add(date);
        dates.add(date);
        dates.add(thirdDate);
        
        AppointmentRequest request = new AppointmentRequest(testDoc, testPatient, dates, testSecretary);
        
        assertEquals(request.getPossibleDates().get(0), date);
        assertEquals(request.getPossibleDates().get(2), thirdDate);
        
        String response = "Accepted";
        testPatient.setResponse(request, response, testSecretary);
        
        assertEquals(request.getRequestStatus(), response);
        
        //should have a notification message about set response
        assertNotNull(testSecretary.getAllNotifications().get(0));
    }

    @Test
    public void testUpdate() {
        testPatient.update();
        
        assertNotNull(testPatient.getAllNotifications().get(0));
    }

    @Test
    public void testCreateRemovalAccountRequest() {
        
        Secretary checkSecretary = testPatient.createRemovalAccountRequest(testSecretary);
        
        assertEquals(checkSecretary.getUserId(), testSecretary.getUserId());
        
        assertEquals(checkSecretary.getAllNotifications().get(0), testSecretary.getAllNotifications().get(0));
    }

    
}
