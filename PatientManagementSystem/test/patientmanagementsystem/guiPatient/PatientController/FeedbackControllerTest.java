/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiPatient.PatientController;

import java.time.LocalDate;
import java.time.Month;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import patientmanagementsystem.adminModel.Admin;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Awaiting;
import patientmanagementsystem.model.Consulted;
import patientmanagementsystem.patientModel.FeedbackRequest;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.secretaryModel.Secretary;

public class FeedbackControllerTest {
    
    private Secretary testSecretary;
    private Doctor testDoc;
    private Patient testPatient;
    private Admin testAdmin;
    
    public FeedbackControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        String first = "David";
        String last = "Dav";
        String userId = "S1234";
        String password = "password";
        String address = "address";
        String postCode = "PL1 123";
        
        testSecretary = new Secretary(first, last, userId, password, address, postCode);
        
        first = "Suzi";
        last = "Suz";
        userId = "D1234";
        password = "password";
        address = "another address";
        postCode = "PL4 123";
        
        testDoc = new Doctor(first, last, userId, password, address, postCode);
        
        first = "Billie";
        last = "Bill";
        userId = "P1235";
        password = "password";
        address = "an address";
        postCode = "PL9 123";
        String gender = "Female";
        LocalDate birthday = LocalDate.of(1999, Month.OCTOBER, 12);
        
        testPatient = new Patient(first, last, userId, password, address, postCode, gender, birthday);
        
        first = "Sarah";
        last = "Sar";
        userId = "A1234";
        password = "password";
        address = "address";
        postCode = "pl1 123";
        
        testAdmin = new Admin(first, last, userId, password, address, postCode);
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testCheckAllowFeedback() {
        
        System.out.println("Test allowing feedback");
        String date = "10/10/10";
        Appointment appointment = new Appointment(testDoc, testPatient, date);
        
        String rating = "Good";
        String comment ="comment";
        
        FeedbackRequest theRequest = appointment.allowFeedback(testDoc, rating, comment);
        
        Awaiting awaiting = new Awaiting();
        
        assertEquals(awaiting.getClass(), appointment.getAppointmentStatus().getClass());
        //Should return null because IState is Awaiting
        assertNull(theRequest);
        
        //Finish appintment to change IState
        String notes = "hjsdhfhdsjhdfjf";
        testDoc.finishAppointment(appointment, notes);
        
        //Check if appointment changed to new state
        Consulted finish = new Consulted();
        assertEquals(appointment.getAppointmentStatus().getClass(), finish.getClass());
        
        //should return as not null
        theRequest = appointment.allowFeedback(testDoc, rating, comment);
        assertNotNull(theRequest);
        
        String notification = "Feedback request";
        //Send request to admin
        testPatient.sendRequest(theRequest, testPatient, testAdmin, notification);
        
        //Check if admin received feedback
        assertEquals(testAdmin.getAllRequests().get(0).getRequestType(), theRequest.getRequestType());
        
        //Check if admin received notification
        assertEquals(testAdmin.getAllNotifications().get(0), notification);
    }

}
