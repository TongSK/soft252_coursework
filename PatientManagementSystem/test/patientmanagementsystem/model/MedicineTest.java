/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.model;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import patientmanagementsystem.patientModel.Patient;

public class MedicineTest {
    
    private Medicine medicine;
    private ArrayList<IObserver> observers = new ArrayList<IObserver>();
    private Patient testPatient;
    private Patient twoPatient;
    private Patient thirdPatient;
    
    public MedicineTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        String first = "Bobby";
        String last = "Bob";
        String userId = "P1234";
        String password = "password";
        String address = "an address";
        String postCode = "PL5 123";
        String gender = "Male";
        LocalDate birthday = LocalDate.of(1999, Month.OCTOBER, 12);
        
        testPatient = new Patient(first, last, userId, password, address, postCode, gender, birthday);
        
        first = "Billie";
        last = "Bill";
        userId = "P1235";
        password = "password";
        address = "an address";
        postCode = "PL9 123";
        gender = "Female";
        birthday = LocalDate.of(1997, Month.OCTOBER, 13);
        
        twoPatient = new Patient(first, last, userId, password, address, postCode, gender, birthday);
        
        first = "Lillie";
        last = "Lill";
        userId = "P1236";
        password = "password";
        address = "an address";
        postCode = "PL3 123";
        gender = "Female";
        birthday = LocalDate.of(1997, Month.APRIL, 13);
        
        
        thirdPatient = new Patient(first, last, userId, password, address, postCode, gender, birthday);
        
        medicine = new Medicine("POTION");
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testNotifyObservers() {
        
        observers.add(testPatient);
        observers.add(twoPatient);
        observers.add(thirdPatient);
        
        if(this.observers != null && observers.size() > 0)
        {
            for(IObserver observer : observers)
            {
                observer.update();
                Patient patient = (Patient) observer;
                assertEquals(patient.getAllNotifications().size(), 1);
            }
        }
    }

    @Test
    public void testRemoveObserver() {
        observers.add(testPatient);
        observers.add(twoPatient);
        observers.add(thirdPatient);
        
        while(observers.size() > 0)
        {
            assertNotNull(observers.get(0));
            observers.remove(0);
        }
        
        assertEquals(observers.size(), 0);
    }

    @Test
    public void testAddObserver() {
        
        assertEquals(testPatient.getFirstName(), "Bobby");
        observers.add(testPatient);
        observers.add(twoPatient);
        observers.add(thirdPatient);
        
        assertEquals(observers.size(), 3);
    }


    
}
