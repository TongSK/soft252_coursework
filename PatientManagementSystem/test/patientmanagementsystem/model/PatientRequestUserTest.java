/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.model;

import java.time.LocalDate;
import java.time.Month;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.secretaryModel.Secretary;


public class PatientRequestUserTest {
    
    private AccountCreationRequest request;
    private PatientRequestUser requestUser;
    private Secretary testSecretary;
    
    public PatientRequestUserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        String first = "Bobby";
        String last = "Bob";
        String address = "an address";
        String postCode = "PL5 123";
        String gender = "Male";
        LocalDate birthday = LocalDate.of(1999, Month.OCTOBER, 12);
        
        first = "David";
        last = "Dav";
        String userId = "S1234";
        String password = "password";
        address = "address";
        postCode = "PL1 123";
        
        testSecretary = new Secretary(first, last, userId, password, address, postCode);
        
        request = new AccountCreationRequest(first, last,address, postCode, gender, birthday);
        requestUser = new PatientRequestUser(request);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSendRequest() {
        
        Secretary anotherSecretary = requestUser.SendRequest(testSecretary);
        
        //Check if request was sent and received
        assertEquals(anotherSecretary.getAllRequests().get(0).getRequestType(), testSecretary.getAllRequests().get(0).getRequestType());
    }
    
}
