/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.doctorModel.OrderRequest;
import patientmanagementsystem.secretaryModel.Secretary;

public class FacadeRequestTest {
    
    private Secretary testSecretary;
    private Doctor testDoc;
    private FacadeRequest facade;
    
    public FacadeRequestTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        String first = "David";
        String last = "Dav";
        String userId = "S1234";
        String password = "password";
        String address = "address";
        String postCode = "PL1 123";
        
        testSecretary = new Secretary(first, last, userId, password, address, postCode);
        
        first = "Suzi";
        last = "Suz";
        userId = "D1234";
        password = "password";
        address = "another address";
        postCode = "PL4 123";
        
        testDoc = new Doctor(first, last, userId, password, address, postCode);
        
        facade = new FacadeRequest(testDoc, testSecretary);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSendFacadeRequest() {
        String name = "medz";
        Medicine medicine = new Medicine(name);
        OrderRequest order = new OrderRequest(medicine);
        String notification = "Notification";
        
        facade.sendFacadeRequest(order, notification);
        
        //Check if secretary received order
        assertEquals(testSecretary.getAllRequests().get(0).getRequestType(), order.getRequestType());
        
        //Check if notified
        assertEquals(testSecretary.getAllNotifications().size(), 1);
        
        OrderRequest checkOrder = (OrderRequest) testSecretary.getAllRequests().get(0);
        
        assertEquals(checkOrder.getMedicine().getName(), order.getMedicine().getName());
    }
    
}
