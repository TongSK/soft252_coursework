package patientmanagementsystem.adminModel;


import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.User;
import patientmanagementsystem.secretaryModel.Secretary;
import patientmanagementsystem.doctorModel.Doctor;
import java.util.ArrayList;

/**
 *
 * @author sheke
 */
public class Admin extends User{
    
    private static final long serialVersionUID = 10L;
    
    /**
     *
     * @param firstName
     * @param lastName
     * @param userId
     * @param password
     * @param address
     * @param postCode
     */
    public Admin(String firstName, String lastName, String userId, String password, String address, String postCode)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userId = userId;
        this.password = password;
        this.address = address;
        this.postCode = postCode;
        
        ArrayList<Request> requests = new ArrayList<Request>();
        ArrayList<String> notifications = new ArrayList<String>();
        this.allNotifications = notifications;
        this.allRequests = requests;
    }
    
    /**
     * Add feedback object to doctor's arrayList of feedback
     * @param doctor
     * @param feedback
     */
    public void giveFeedback(Doctor doctor, Feedback feedback) {
        doctor.receiveFeedback(feedback);
    }

    /**
     * //Check first character of userId to determine which user object we create
     * @param firstName
     * @param LastName
     * @param userId
     * @param password
     * @param address
     * @param postCode
     */
    public void createAccount(String firstName, String lastName, String userId, String password, String address, String postCode) {
        char firstChar = userId.charAt(0); 
        
        if(firstChar == 'A')
        {
            Admin newAdmin = new Admin(firstName, lastName, userId, password, address, postCode);
        }
        else if(firstChar == 'D')
        {
            Doctor newDoctor = new Doctor(firstName, lastName, userId, password, address, postCode);
        }
        else if(firstChar == 'S')
        {
            Secretary newSecretary = new Secretary(firstName, lastName, userId, password, address, postCode);
        }
        
    }
}
