package patientmanagementsystem.adminModel;


import java.io.Serializable;
import patientmanagementsystem.doctorModel.Doctor;


/**
 *
 * @author sheke
 */
public class Feedback implements Serializable{
    
    private static final long serialVersionUID = 8L;
    private Doctor doctor;
    private String rating;
    private String comment;
    
    /**
     *
     * @param theDoctor
     * @param theRating
     * @param theComment
     */
    public Feedback(Doctor theDoctor, String theRating, String theComment) {
        this.doctor = theDoctor;
        this.rating = theRating;
        this.comment = theComment;
    }
    
    //GETTERS AND SETTERS

    /**
     *
     * @return
     */

    public Doctor getDoctor() {
        return doctor;
    }

    /**
     *
     * @param doctor
     */
    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    /**
     *
     * @return
     */
    public String getRating() {
        return rating;
    }

    /**
     *
     * @param rating
     */
    public void setRating(String rating) {
        this.rating = rating;
    }

    /**
     *
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }
    
    

}
