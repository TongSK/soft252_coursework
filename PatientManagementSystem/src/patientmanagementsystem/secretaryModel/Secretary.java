package patientmanagementsystem.secretaryModel;
import patientmanagementsystem.doctorModel.Prescription;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.User;
import patientmanagementsystem.model.Medicine;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.doctorModel.Doctor;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author shekei
 */
public class Secretary extends User {
    
    private static final long serialVersionUID =12L;

    /**
     *
     * @param firstName
     * @param lastName
     * @param userId
     * @param password
     * @param address
     * @param postCode
     */
    public Secretary(String firstName, String lastName, String userId, String password, String address, String postCode) {
        
        this.firstName = firstName;
        this.lastName = lastName;
        this.userId = userId;
        this.password = password;
        this.address = address;
        this.postCode = postCode;
        
        ArrayList<Request> requests = new ArrayList<Request>();
        ArrayList<String> notifications = new ArrayList<String>();
        this.allNotifications = notifications;
        this.allRequests = requests;
        
    }
    
    //Create new Appointment object and then add to patient's arraylist of Appointment objects
    /**
     *
     * @param doctor
     * @param patient
     * @param dateTime - the agreed date of appointment
     */
    public void createAppointment(Doctor doctor, Patient patient, String dateTime) {
        Appointment newAppointment = new Appointment(doctor, patient, dateTime);
        sendAppointment(patient, newAppointment);
    }
    
    //Add appointment to patient's appointment arraylist
    public void sendAppointment(Patient patient, Appointment appointment) {
        patient.receiveAppointment(appointment);
    }
    
    
    //Create a new Patient object
    public void createPatientAccount(String firstName, String lastName, String userId, String password, String address, String postCode, String gender, LocalDate dateOfBirth)
    {
        Patient newPatient = new Patient(firstName, lastName, userId, password, address, postCode, gender, dateOfBirth);
    }

    /**
     *
     * @param medicine what the patient needs
     * @param prescribedMed
     * @param patient of the prescribed med
     */
    public Patient giveMedicine(Medicine medicine, Appointment appointment, int prescribedIndex, int appointmentIndex) {
        
        Prescription prescribedMed = appointment.getPrescribedMedicine().get(prescribedIndex);
        Patient patient = appointment.getPatient();
        ArrayList<Appointment> allAppointments = patient.getAllAppointments();
        ArrayList<Prescription> appointmentPrescriptions = patient.getAllAppointments().get(appointmentIndex).getPrescribedMedicine();
        
        if(prescribedMed.getQuantity() < medicine.getStock())
        {
            int amount = prescribedMed.getQuantity() * -1; 
            updateMedStock(medicine, amount); //Decrease current stock of medicine
            prescribedMed.setReceived(Boolean.TRUE); //Set prescribed to medicine as received
            appointmentPrescriptions.remove(prescribedIndex); //Remove old prescription object with old status
            appointmentPrescriptions.add(prescribedMed);//Add new status object
            appointment.setPrescribedMedicine(appointmentPrescriptions);//Set updated prescription arrayList
            allAppointments.remove(appointmentIndex);//Remove old appointment object with old prescription
            allAppointments.add(appointment);
            patient.setAllAppointments(allAppointments); //Set updated appointment arrayList
            medicine.removeObserver(patient); //Remove patient from notification system if necessary
        }
        else
        {
            medicine.addObserver(patient); //Add patient to list of observers to notify when restock happens
        }
        
        return patient;
    }

    //Increase or decrease med stock
    public Medicine updateMedStock(Medicine med, int quantity) {
        med.setStock(quantity);
        
        return med;
    }

   
}
