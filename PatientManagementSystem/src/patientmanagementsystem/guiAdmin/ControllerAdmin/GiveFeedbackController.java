/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiAdmin.ControllerAdmin;

import java.util.ArrayList;
import patientmanagementsystem.adminModel.Admin;
import patientmanagementsystem.adminModel.Feedback;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.guiAdmin.SendFeedback;
import patientmanagementsystem.guiAdmin.ViewFeedback;
import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.patientModel.FeedbackRequest;

/**
 *
 * @author sheke
 */
public class GiveFeedbackController {
    
    private Singleton singleton = Singleton.getInstance();
    
    public GiveFeedbackController()
    {
    
    }
    
    public void toSendFeedback(int requestIndex, Admin theAdmin, ViewFeedback thisPage)
    {
        SendFeedback feedbackPage = new SendFeedback(theAdmin, requestIndex);
        feedbackPage.setVisible(true);
        thisPage.dispose();
    }
    
    public void sendFeedback(Admin theAdmin, String comment, int requestIndex, SendFeedback currentPage)
    {
        ArrayList<Request> allFeedback = theAdmin.getAllRequests();
        FeedbackRequest feedbackRequest = (FeedbackRequest)allFeedback.get(requestIndex); //Get specified request
        feedbackRequest.setComment(comment); //Alter comment attribute if admin editted comment input
        Doctor doctor = feedbackRequest.getDoctor(); //Get rated doctor
        
        //Create feedback object using inputs from feedback request object
        Feedback feedback = new Feedback(feedbackRequest.getDoctor(), feedbackRequest.getRating(), feedbackRequest.getComment());
        
        theAdmin.giveFeedback(doctor, feedback); //Doctor receives feedback with notificaton
        
        singleton.saveUser(doctor); //Save doctor with new feedback
        
        allFeedback.remove(requestIndex); //Remove request after conversion
        theAdmin.setAllRequests(allFeedback); //Set back into admin
        singleton.saveUser(theAdmin); //Save admin with new set
        
        toViewFeedback(currentPage, theAdmin);
        
    }
    
    public void toViewFeedback(SendFeedback thisPage, Admin theAdmin)
    {
        ViewFeedback backPage = new ViewFeedback(theAdmin);
        backPage.setVisible(true);
        thisPage.dispose();
    }
}
