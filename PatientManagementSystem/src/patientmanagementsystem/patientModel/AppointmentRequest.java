package patientmanagementsystem.patientModel;


import patientmanagementsystem.model.Request;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.doctorModel.Doctor;
import java.util.ArrayList;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author sheke
 */
public class AppointmentRequest extends Request {

    private Doctor doctor;
    private Secretary secretary;
    private Patient patient;

    private ArrayList<String> possibleDates;

    private String requestStatus;
    
    private String proposedDate;

    /**
     *
     * @param theDoctor
     * @param thePatient
     * @param dates
     */
    public AppointmentRequest(Doctor theDoctor, Patient thePatient, ArrayList<String> dates, Secretary theSecretary) {
        
        this.requestType = "APPOINTMENT REQUEST";
        this.requestStatus = "PATIENT REQUEST";
        this.proposedDate = "NOT YET PROPOSED";
        
        this.doctor = theDoctor;
        this.patient = thePatient;
        this.possibleDates = dates;
        this.secretary = theSecretary;
        
    }
    
    //GETTERS AND SETTERS

    /**
     *
     * @return
     */
    
    public Doctor getDoctor() {
        return doctor;
    }

    /**
     *
     * @param doctor
     */
    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    /**
     *
     * @return
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     *
     * @return
     */
    public ArrayList<String> getPossibleDates() {
        return possibleDates;
    }

    /**
     *
     * @param possibleDates
     */
    public void setPossibleDates(ArrayList<String> possibleDates) {
        this.possibleDates = possibleDates;
    }

    /**
     *
     * @return
     */
    public String getRequestStatus() {
        return requestStatus;
    }

    /**
     *
     * @param requestStatus
     */
    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    /**
     *
     * @return
     */
    public String getProposedDate() {
        return proposedDate;
    }

    /**
     *
     * @param proposedDate
     */
    public void setProposedDate(String proposedDate) {
        this.proposedDate = proposedDate;
    }

    public Secretary getSecretary() {
        return secretary;
    }

    public void setSecretary(Secretary secretary) {
        this.secretary = secretary;
    }
    
    
    
}
