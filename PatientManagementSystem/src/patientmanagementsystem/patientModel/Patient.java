package patientmanagementsystem.patientModel;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.User;
import patientmanagementsystem.secretaryModel.Secretary;
import patientmanagementsystem.model.IObserver;
import patientmanagementsystem.model.IRespond;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author shekei
 */
public class Patient extends User implements IRespond, IObserver {
    
    private static final long serialVersionUID = 13L;

    private LocalDate dateOfBirth;

    private String gender;

    private ArrayList<Appointment> allAppointments;
    
    /**
     *
     * @param firstName
     * @param lastName
     * @param userId
     * @param password
     * @param address
     * @param postCode
     * @param gender
     * @param dateOfBirth
     */
    public Patient(String firstName, String lastName, String userId, String password, String address, String postCode, String gender, LocalDate dateOfBirth)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userId = userId;
        this.password = password;
        this.address = address;
        this.postCode = postCode;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        
        ArrayList<Request> requests = new ArrayList<Request>();
        ArrayList<String> notifications = new ArrayList<String>();
        this.allNotifications = notifications;
        this.allRequests = requests;
        
        ArrayList<Appointment> appointments = new ArrayList<Appointment>();
        this.allAppointments = appointments;
        
    }
    
    /**
     *
     * @param appointment
     */
    public void receiveAppointment(Appointment appointment) {
        allAppointments.add(appointment); //Add to list of appointments
        String notification = LocalDate.now().toString() + "APPOINTMENT HAS BEEN CREATED FOR YOU.";
        notify(notification); //Notify patient of appointment creation
    }
    
    //Output arrayList of Appointment objects

    /**
     * 
     * @param appointment
     * @return arrayList of Appointment objects
     */
    public ArrayList<Appointment> viewAllAppointments(ArrayList<Appointment> appointment) {
        return this.allAppointments;
    }

    /**
     *
     * @param theRequest appointment request
     * @param response whether patient accepts
     * 
     */
    @Override
    public void setResponse(AppointmentRequest theRequest, String response, Secretary secretary) {
        theRequest.setRequestStatus(response);
        String notification = LocalDate.now().toString() + " " + this.getUserId() + " - APPOINTMENT REQUEST RESPONSE.";
        this.sendRequest(theRequest, secretary, this, notification);
    }

    /**
     * Notify patient when a medicine they have been prescribed but not yet received has been re-stocked
     * Observer pattern 
     */
    @Override
    public void update() {
        String notification = LocalDate.now().toString() + " - PRESCRIBED MEDICINE HAS BEEN RESTOCKED.";
        notify(notification);
    }
    
    //Create a remove account request and send to secretary for approval
    public Secretary createRemovalAccountRequest(Secretary secretary)
    {
        AccountRemovalRequest request = new AccountRemovalRequest(this);
        String notification = LocalDate.now().toString() + " " + this.getUserId() + " - REMOVAL ACCOUNT REQUEST";
        this.sendRequest(request, secretary, this, notification);
        
        return secretary;
        
    }
    
    //GETTERS AND SETTERS

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ArrayList<Appointment> getAllAppointments() {
        return allAppointments;
    }

    public void setAllAppointments(ArrayList<Appointment> allAppointments) {
        this.allAppointments = allAppointments;
    }
    
    
    
    
}
