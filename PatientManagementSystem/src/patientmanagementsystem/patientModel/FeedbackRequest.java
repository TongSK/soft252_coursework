package patientmanagementsystem.patientModel;


import patientmanagementsystem.model.Request;
import patientmanagementsystem.doctorModel.Doctor;


/**
 *
 * @author shekei
 */
public class FeedbackRequest extends Request {

    private Doctor doctor;
    private String rating;
    private String comment;

    /**
     *
     * @param theDoctor
     * @param theRating
     * @param theComment
     */
    public FeedbackRequest(Doctor theDoctor, String theRating, String theComment) {
        
        this.requestType = "FEEDBACK REQUEST";
        
        this.doctor = theDoctor;
        this.rating = theRating;
        this.comment = theComment;
    }
    
    //GETTERS AND SETTERS

    /**
     *
     * @return
     */
    
    public Doctor getDoctor() {
        return doctor;
    }

    /**
     *
     * @param doctor
     */
    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    /**
     *
     * @return
     */
    public String getRating() {
        return rating;
    }

    /**
     *
     * @param rating
     */
    public void setRating(String rating) {
        this.rating = rating;
    }

    /**
     *
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }
    
    
}
