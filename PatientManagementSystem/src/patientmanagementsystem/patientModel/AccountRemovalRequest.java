package patientmanagementsystem.patientModel;


import patientmanagementsystem.model.Request;
import patientmanagementsystem.patientModel.Patient;


/**
 *
 * @author sheke
 */
public class AccountRemovalRequest extends Request {

    private Patient patient;

    /**
     *
     * @param thePatient
     */
    public AccountRemovalRequest(Patient thePatient) {
        
        this.requestType = "REMOVE REQUEST";
        this.patient = thePatient;
    }

    /**
     *
     * @return
     */
    public Patient getPatient() {
        return patient;
    }
    
}
