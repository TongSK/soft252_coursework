package patientmanagementsystem.doctorModel;
import patientmanagementsystem.adminModel.Feedback;
import patientmanagementsystem.model.Consulted;
import patientmanagementsystem.patientModel.AppointmentRequest;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.User;
import patientmanagementsystem.model.Medicine;
import patientmanagementsystem.secretaryModel.Secretary;
import patientmanagementsystem.model.IRespond;
import patientmanagementsystem.model.IState;
import java.time.LocalDate;
import java.util.ArrayList;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.patientModel.Patient;

public class Doctor extends User implements IRespond {
    
    private static final long serialVersionUID = 11L;
    private ArrayList<Feedback> allFeedback;
    
    /**
     *
     * @param firstName
     * @param lastName
     * @param userId
     * @param password
     * @param address
     * @param postCode
     */
    public Doctor (String firstName, String lastName, String userId, String password, String address, String postCode) {
        
        this.firstName = firstName;
        this.lastName = lastName;
        this.userId = userId;
        this.password = password;
        this.address = address;
        this.postCode = postCode;
        
        ArrayList<Request> requests = new ArrayList<Request>();
        ArrayList<String> notifications = new ArrayList<String>();
        this.allNotifications = notifications;
        this.allRequests = requests;
        
        ArrayList<Feedback> feedback = new ArrayList<Feedback>();
        this.allFeedback = feedback;
        
    }
     
    //Add feedback object to arrayList of Feedback and notify doctor of receival 

    /**
     *
     * @param feedback
     */
    public void receiveFeedback(Feedback feedback) {
        allFeedback.add(feedback);
        String notification = LocalDate.now().toString() + "RECEIVED FEEDBACK.";
        notify(notification);
    }

    /**
     *
     * @param medName
     * @param secretary
     */
    public Medicine createMedicine(String medName, Secretary secretary) {
        Medicine newMedicine = new Medicine(medName);//Create new medicine object and set stock to 0
        OrderRequest request = new OrderRequest(newMedicine); //Create an order request of new medicine to be sent to secretary
        String notification = LocalDate.now().toString() + "ORDER REQUEST MADE.";
        
        this.sendRequest(request, secretary, this, notification);
        
        return newMedicine;
        
    }

    /**
     *
     * @param appointment the appointment we are giving prescribed medicine for
     * @param medicine
     * @param quantity
     * @param dosage
     * @return
     */
    public Appointment createPrescription(Appointment appointment, Medicine medicine, int quantity, String dosage) {
        Prescription prescription = new Prescription(medicine,quantity,dosage); //Create new prescribed medicine
        ArrayList<Prescription> currentPrescription = appointment.getPrescribedMedicine(); //Get arrayList of current prescription
        currentPrescription.add(prescription); //Add new prescription to arrayList
        appointment.setPrescribedMedicine(currentPrescription); //Save arrayList
        
        return appointment;
    }

    /**
     *
     * @param appointment
     * @param notes
     * @return
     */
    public Patient finishAppointment(Appointment appointment, String notes) {
        appointment.setConsultationNotes(notes); //Save notes written during the appointment
        IState finished = new Consulted();
        appointment.setAppointmentStatus(finished); //Change status of appointment as finished
        
        if(appointment.getPrescribedMedicine().size() > 0) //If prescribed medicine
        {
            appointment.setPrescriptionStatus("NOT YET RECEIVED");
        }
        
        Patient patient = appointment.getPatient();
        String notification = LocalDate.now() +" - "+ appointment.getDoctor().getUserId() +" - has FINISHED CONSULTATION, you may now provide FEEDBACK ";
        patient.notify(notification); //Notify patient
        
        ArrayList<Appointment> allAppointments = patient.getAllAppointments();
        
        //Find old appointment to replace by using date, doctor and patient id
        for (int i = 0; i < allAppointments.size(); i++) {
            Appointment checkAppointment = allAppointments.get(i);
            if(checkAppointment.getDateTime().equals(appointment.getDateTime()) == true && checkAppointment.getDoctor().getUserId().equals(appointment.getDoctor().getUserId()) == true && checkAppointment.getPatient().getUserId().equals(appointment.getPatient().getUserId()) == true)
            {
                allAppointments.remove(i); //Remove current appointment with old status
                break;
            }
        }
        
        allAppointments.add(appointment);//Replace with same appointment but new status
        patient.setAllAppointments(allAppointments); //Save back to user
        
        return patient;
        
    }

    /**
     * Set a proposal date for an appointment request
     * Send to secretary for review
     * @param theRequest the appointment request
     * @param response the proposed date for appointment
     * @param secretary
     */
    @Override
    public void setResponse(AppointmentRequest theRequest, String response, Secretary secretary) {
        theRequest.setProposedDate(response);
        theRequest.setRequestStatus("DOCTOR PROPOSAL"); //Change status to show proposal has been made
        String notification = LocalDate.now().toString() + " " +  this.getUserId() + " - APPOINTMENT REQUEST RESPONSE.";
        this.sendRequest(theRequest, secretary, this, notification);
        
    }
    
    //GETTERS AND SETTERS
    
    public ArrayList<Feedback> getAllFeedback() {
        return allFeedback;
    }

    public void setAllFeedback(ArrayList<Feedback> allFeedback) {
        this.allFeedback = allFeedback;
    }
    
    
    
}
