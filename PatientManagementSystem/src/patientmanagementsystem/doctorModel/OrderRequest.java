package patientmanagementsystem.doctorModel;


import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.Medicine;


/**
 *
 * @author sheke
 */
public class OrderRequest extends Request {

    private Medicine medicine;

    /**
     *
     * @param theMedicine
     */
    public OrderRequest(Medicine theMedicine) {
        
        this.requestType = "ORDER REQUEST";
        this.medicine = theMedicine;
    }

    /**
     *
     * @return
     */
    public Medicine getMedicine() {
        return medicine;
    }
    
}
