package patientmanagementsystem.doctorModel;


import java.io.Serializable;
import patientmanagementsystem.model.Medicine;


/**
 *
 * @author sheke
 */
public class Prescription implements Serializable {
    
    private static final long serialVersionUID = 9L;

    private Medicine medicine;
    private int quantity;
    private String dosage;
    private Boolean received;
    
    /**
     *
     * @param theMedicine
     * @param theQuantity
     * @param theDosage
     * @param receival whether medicine has been given to them by a secretary - set to default not yet received
     */
    public Prescription(Medicine theMedicine, int theQuantity, String theDosage)
    {
        this.medicine = theMedicine;
        this.quantity = theQuantity;
        this.dosage = theDosage;
        this.received = false; 
    }
    
    //GETTERS AND SETTERS

    /**
     *
     * @return 
     */

    public Medicine getMedicine() {
        return medicine;
    }

    /**
     *
     * @param medicine
     */
    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    /**
     *
     * @return
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     *
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     *
     * @return
     */
    public String getDosage() {
        return dosage;
    }

    /**
     *
     * @param dosage
     */
    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    /**
     *
     * @return
     */
    public Boolean getReceived() {
        return received;
    }

    /**
     *
     * @param received
     */
    public void setReceived(Boolean received) {
        this.received = received;
    }
    
    
}
