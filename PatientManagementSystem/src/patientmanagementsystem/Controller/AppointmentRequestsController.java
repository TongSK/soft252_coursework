/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.Controller;

import java.util.ArrayList;
import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.model.User;
import patientmanagementsystem.patientModel.AppointmentRequest;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author shekei
 */
public class AppointmentRequestsController {
    Singleton singleton = Singleton.getInstance();
    
    public AppointmentRequestsController()
    {
    
    }
    
    //Return with user's arrayList of request objects
    public ArrayList<Request> getRequestArrayList(User theUser)
    {
        ArrayList<Request> allRequests = theUser.getAllRequests();
        
        return allRequests;
    }
    
    //Return arrayList of request objects of a certain type
    public ArrayList<Request> filterAppointmentRequests(String status, ArrayList<Request> allRequests)
    {
        ArrayList<Request> filterRequests = new ArrayList<Request>();
        
        for (int i = 0; i < allRequests.size(); i++) {
            
            AppointmentRequest requestTemp = (AppointmentRequest)allRequests.get(i);
            String requestType = requestTemp.getRequestType(); //Get the type of request
            
            if(requestType.equals("APPOINTMENT REQUEST") == true) //Get request objects that are only appointment requests
            {
                AppointmentRequest appointment = (AppointmentRequest) allRequests.get(i); 
                
                if(appointment.getRequestStatus().equals(status) == true) //If appointment has the same status as the input status
                {
                    filterRequests.add(appointment); //Add to the output parameter arrayList
                }
            }
        }
        
        return filterRequests;
    }
    
    //Remove the object from filtered arrayList and then remove from the overall ArrayList of user
    public Secretary removeRequest(ArrayList<Request> theRequests, int index, Secretary user)
    {
        AppointmentRequest removeRequest = (AppointmentRequest) theRequests.get(index);
        theRequests.remove(index);
        user = saveAppointmentRemoval(user, removeRequest); //remove same object from the overall ArrayList of user that contains other types of requests
        singleton.saveUser(user);
        return user;
    }
    
    public Secretary saveAppointmentRemoval(Secretary theUser, AppointmentRequest appointment)
    {
        boolean allMatch = true;
        
        ArrayList<Request> theRequests = theUser.getAllRequests(); //All the request objects from User object
        
        String appointmentDoctor = appointment.getDoctor().getUserId(); //The doctor id we need to use to find and remove
        String appointmentPatient = appointment.getPatient().getUserId();
        ArrayList<String> appointmentDates = appointment.getPossibleDates();
        
        for (int i = 0; i < theRequests.size(); i++) {
            
            AppointmentRequest fromArray = (AppointmentRequest) theRequests.get(i); //Info from each object
            String patientId = fromArray.getPatient().getUserId();
            String doctorId = fromArray.getDoctor().getUserId();
            ArrayList<String> fromArrayDates = fromArray.getPossibleDates();
            
            if(appointmentDoctor.equals(doctorId) == true && appointmentPatient.equals(patientId) == true) //If doctor and patient object match the specification
            {
                allMatch = compareDates(fromArrayDates, appointmentDates); //Check appointment dates
                
                if(allMatch == false)
                {
                    theRequests.remove(i); //Remove request object
                    theUser.setAllRequests(theRequests); //Save back to user
                    break; //Immediately stop loop
                }
            }
        }
        
        return theUser;
    }
    
    public boolean compareDates(ArrayList<String> datesFromArray, ArrayList<String> appointmentTimes)
    {
        boolean allMatch = true;
        
        for (int i = 0; i < datesFromArray.size(); i++) {
            
            if(datesFromArray.get(i).equals(appointmentTimes.get(i)) == false) //If dates do not match
            {
                allMatch = false;
                break;
            }
            
        }
        
        return allMatch;
    }
}
