/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.Controller;

import java.util.ArrayList;

/**
 *
 * @author shekei
 * 
 */
public class InputController {
    
    public InputController()
    {
    
    }
    
    //Check for whether all inputs has been filled in
    public boolean checkInput(ArrayList<String> theInputs)
    {
        boolean isEmpty = false;
        for (int i = 0; i < theInputs.size(); i++) {
            
            if(theInputs.get(i).equals("") == true)
            {
                isEmpty = true;
                break; //Imediately stop once an empty field is found
            }
        }
        
        return isEmpty;
    }
    
    //Check whether first char of user id begins with correct letter
    public boolean checkUserId(String userId, String accountType)
    {
        boolean correctId = true;
        char userChar = userId.charAt(0);
        char accountChar = accountType.charAt(0);
        
       if(userChar != accountChar)
       {
           correctId = false;
       }
       
       return correctId;
    }
    
    //Check if userId is 5 characters long
    public boolean checkUserIdLength(String userId)
    {
        boolean correctLength = true;
        
        if(userId.length() != 5)
        {
            correctLength = false;
        }
       
        return correctLength;
    
    }
    
}
