/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.Controller;

import java.util.ArrayList;
import patientmanagementsystem.adminModel.Admin;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.model.User;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author sheke
 */
public class RemoveAccountController {
    
     Singleton singleton = Singleton.getInstance();
     
     public RemoveAccountController()
     {
     
     }
     
     public void removeAdminAccount(Admin admin)
     {
         ArrayList<User> currentAdmins = singleton.getAdmin();
         currentAdmins = removeUser(currentAdmins,admin);
         singleton.setAdmin(currentAdmins);
     }
     
     public void removeDoctorAccount(Doctor doctor)
     {
         ArrayList<User> currentDoctors = singleton.getDoctors();
         currentDoctors = removeUser(currentDoctors, doctor);
         singleton.setDoctors(currentDoctors);
     }
     
     public void removeSecretaryAccount(Secretary secretary)
     {
         ArrayList<User> currentSecretaries = singleton.getSecretary();
         currentSecretaries = removeUser(currentSecretaries, secretary);
         singleton.setSecretary(currentSecretaries);
     }
     
     public ArrayList<User> removeUser(ArrayList<User> allUsers, User theUser)
     {
         for (int i = 0; i < allUsers.size(); i++) {
             
             if(theUser.getUserId().equals(allUsers.get(i).getUserId())== true)
             {
                 allUsers.remove(i);
                 break;
             }
         }
         
         return allUsers;
     }
     
     
    
}
