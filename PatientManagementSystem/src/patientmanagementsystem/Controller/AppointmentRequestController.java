/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.Controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.model.User;
import patientmanagementsystem.patientModel.AppointmentRequest;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author shekei
 */
public class AppointmentRequestController {
    
    private GetUserController userController = new GetUserController();
    private Singleton singleton = Singleton.getInstance();
    
    public AppointmentRequestController()
    {
    
    }
    
    //Selected string from doctors combo box goes = doctor.getId() + " Dr " + doctor.getLastName()
    //Split the first 5 characters to get just the id
    public Doctor findDoctor(String doctorChoice)
    {
        String theId = Character.toString(doctorChoice.charAt(0)) + Character.toString(doctorChoice.charAt(1)) + Character.toString(doctorChoice.charAt(2)) + Character.toString(doctorChoice.charAt(3)) + Character.toString(doctorChoice.charAt(4));
        Doctor theDoctor = userController.getDoctor(theId);
        
        return theDoctor;
    }
    
    //Patient creates the request
    public void createRequest(Patient patient, String doctorSelected, ArrayList<String> possibleDates)
    {
        Doctor doctor = findDoctor(doctorSelected);
        Secretary secretary = userController.getRandomSecretary();
        AppointmentRequest request = new AppointmentRequest(doctor, patient, possibleDates, secretary);
        String notification = LocalDate.now() + " - " + patient.getUserId() + " has sent an APPOINTMENT REQUEST";
        patient.sendRequest(request, secretary, patient, notification);
        singleton.saveUser(secretary);
    }
    
    //Convert an appointment request to an appointment object by using the attributes already in request object
    //Notify patient and doctor involved
    public void turnRequestToAppointment(AppointmentRequest theRequest)
    {
        Appointment appointment = new Appointment(theRequest.getDoctor(), theRequest.getPatient(), theRequest.getProposedDate());
        theRequest.getSecretary().sendAppointment(theRequest.getPatient(), appointment);
        String notification = LocalDate.now() + " - " + theRequest.getSecretary().getUserId() + " HAS CREATED AN APPOINTMENT FOR YOU";
        theRequest.getDoctor().notify(notification);
        theRequest.getPatient().notify(notification);
        singleton.saveUser(theRequest.getDoctor());
        singleton.saveUser(theRequest.getPatient());
        
    }
    
}
