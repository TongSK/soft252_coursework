/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.Controller;

import java.time.LocalDate;
import patientmanagementsystem.model.AccountCreationRequest;
import patientmanagementsystem.model.PatientRequestUser;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author shekei
 */
public class CreateAccountRequestController {
    
    private GetUserController controller = new GetUserController();
    private Singleton singleton = Singleton.getInstance();
    
    public CreateAccountRequestController()
    {
        
    }
    
    //Create a CreateAccountRequest object and a request user to send to secretary
    public void createRequest(String firstName, String lastName, String theAddress, String thePostCode, String theGender, LocalDate dob)
    {
        
        AccountCreationRequest request = new AccountCreationRequest(firstName,lastName, theAddress,thePostCode,theGender,dob);
        PatientRequestUser requestUser = new PatientRequestUser(request); 
        
        Secretary theSecretary = controller.getRandomSecretary();
        theSecretary = requestUser.SendRequest(theSecretary);
        singleton.saveUser(theSecretary); //Save secretary with new request back into JSON
    }
    
    
    
}
