/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.Controller;

import java.util.ArrayList;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.model.User;

/**
 *
 * @author shekei
 */
public class NotificationsController {
    
    private Singleton singleton = Singleton.getInstance();
    
    public NotificationsController()
    {
    
    }
    
    //NotificationIndex used to find and remove the one String in arrayList
    public void RemoveNotification(User user, int notificationIndex)
    {
        ArrayList<String> allNotifications = user.getAllNotifications(); 
        allNotifications.remove(notificationIndex);
        user.setAllNotifications(allNotifications); //Set updated arrayList back into user
        singleton.saveUser(user); //Save user back into json
    }
    
}
