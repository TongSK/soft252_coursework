/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.Controller;

import java.util.ArrayList;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.model.User;

/**
 *
 * @author shekei
 */
public class PasswordController {
    
    private ArrayList<User> allUsers = new ArrayList<User>();
    private Singleton singleton = Singleton.getInstance();
    
    public PasswordController()
    {
        
    }
    
    public User loadFiles(String userType, String theUsername, String thePassword)
    {
        User userObject = null;
        
        
        //Determine which arrayList we need to get
        if(userType == "Admin")
        {
            this.allUsers = singleton.getAdmin();
        }
        else if(userType == "Doctor")
        {
            this.allUsers = singleton.getDoctors();
        }
        else if(userType == "Patient")
        {
            this.allUsers = singleton.getPatients();
        }
        else
        {
            this.allUsers = singleton.getSecretary();
        }
        
        //Look for username existing first
        User thisUser = findUsername(this.allUsers, theUsername);
        
        //If username does not exist
        if(thisUser == null)
        {
            return userObject; //Return with null userObject
        }
        else //If username does exist
        {
            if(thePassword.equals(thisUser.getPassword()) == true) //And password input is same as user object's password
            {
                userObject = thisUser; //Set userObject to equal output object of findUsername
            }
        }
        
        return userObject;
    }
    
    //Loop through arrayList to check if UserId exists
    public User findUsername(ArrayList<User> theUsers, String username)
    {
        User ourUser = null;
        
        for (int i = 0; i < theUsers.size(); i++) {
            
            String compareId = theUsers.get(i).getUserId();
            
            if ( username.equals(compareId) == true) //Exit loop once input is found in arrayList
            {
                ourUser = theUsers.get(i);
                break;
            }
            
        }
        return ourUser;       
    }
    
    public boolean changePassword(String newPassword, User user)
    {
        boolean empty = false;
        
        if(newPassword.equals("") == true)
        {
            empty = true;
        }
        else
        {
            user.setPassword(newPassword);
            singleton.saveUser(user);
        }
        
        return empty;
        
    }
    
   
    
}
