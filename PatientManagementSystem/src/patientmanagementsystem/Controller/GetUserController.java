/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.Controller;

import java.util.ArrayList;
import java.util.Random;
import patientmanagementsystem.adminModel.Admin;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.model.User;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author sheke
 */
public class GetUserController {
    Singleton singleton = Singleton.getInstance();
    
    public  GetUserController()
    {
    
    }
    
    //Get arrayList and pass into getUser and return with admin object
    public Admin getAdmin(String userId)
    {
        Admin admin = null;
        ArrayList<User> currentAdmins = singleton.getAdmin();
        admin = (Admin) getUser(userId, currentAdmins);
        
        return admin;
        
    }
    
    //Get arrayList and pass into getUser and return with doctor object
    public Doctor getDoctor(String userId)
    {
        Doctor doctor = null;
        ArrayList<User> currentDoctors = singleton.getDoctors();
        doctor = (Doctor) getUser(userId, currentDoctors);
        return doctor;
    }
    
    //Get arrayList and pass into getUser and return with admin object
    public Secretary getSecretary(String userId)
    {
        Secretary secretary = null;
        ArrayList<User> currentSecretaries = singleton.getSecretary();
        secretary = (Secretary) getUser(userId, currentSecretaries);
        return secretary;
    }
    
    //Pass arrayList and find the user within using the id
    public User getUser(String id, ArrayList<User> users)
    {
        User thisUser = null;
        
        for (int i = 0; i < users.size() ; i++) {
            
            if(id.equals(users.get(i).getUserId()))
            {
                thisUser = users.get(i);
            }
        }
        
        return thisUser;
    }
    
    //Decide which secretary gets to receive a request
    public Secretary getRandomSecretary()
    {
        ArrayList<User> secretaries = singleton.getSecretary();
        Secretary secretary = null;
        
        if(secretaries.size() > 1) //If more than one secretary, select at random to receive request
        {
            Random rand = new Random();
            int index = rand.nextInt(secretaries.size());
            secretary = (Secretary) secretaries.get(index);
        }
        else if(secretaries.size() == 1)
        {
            secretary = (Secretary)secretaries.get(0);
        }
        
        return secretary;
    }
    
     public Admin getRandomAdmin()
    {
        ArrayList<User> admins = singleton.getAdmin();
        Admin admin = null;
        
        if(admins.size() > 1) //If more than one secretary, select at random to receive request
        {
            Random rand = new Random();
            int index = rand.nextInt(admins.size());
            admin = (Admin) admins.get(index);
        }
        else if(admins.size() == 1)
        {
            admin = (Admin)admins.get(0);
        }
        
        return admin;
    }
    
    
   
    
    
    
}
