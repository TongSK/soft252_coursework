/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.Controller;

import java.time.LocalDate;
import java.util.ArrayList;
import patientmanagementsystem.adminModel.Admin;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.model.User;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author shekei
 */
public class CreateAccountController {
    
    Singleton singleton = Singleton.getInstance();
    
    public CreateAccountController()
    {
    
    }
    
    //Create new user object
    //Copy out the appropiate arraylist from Singleton
    //Add new object to copied out arrayList
    //Set the arrayList with new object back into Singleton
    
    public void createAdminAccount(String firstName, String lastName, String userId, String password, String address, String postCode)
    {
        Admin newAdmin = new Admin(firstName, lastName, userId, password, address, postCode);
        ArrayList<User> currentAdmins = singleton.getAdmin();
        currentAdmins.add(newAdmin);
        singleton.setAdmin(currentAdmins);
    }
    
    public void createDoctorAccount(String firstName, String lastName, String userId, String password, String address, String postCode)
    {
        Doctor newDoctor = new Doctor(firstName, lastName, userId, password, address, postCode);
        ArrayList<User> currentDoctors = singleton.getDoctors();
        currentDoctors.add(newDoctor);
        singleton.setDoctors(currentDoctors);
    }
    
    public void createSecretaryAccount(String firstName, String lastName, String userId, String password, String address, String postCode)
    {
        Secretary newSecretary = new Secretary(firstName, lastName, userId, password, address, postCode);
        ArrayList<User> currentSecretaries = singleton.getSecretary();
        currentSecretaries.add(newSecretary);
        singleton.setSecretary(currentSecretaries);
    }
    
    public void createPatientAccount(String firstName, String lastName, String userId, String password, String address, String postCode, String gender, LocalDate dob)
    {
        Patient newPatient = new Patient(firstName, lastName, userId, password, address, postCode, gender, dob);
        ArrayList<User> currentPatients = singleton.getPatients();
        currentPatients.add(newPatient);
        singleton.setPatients(currentPatients);
        singleton.saveAll();
    }
    
    
    
    
}
