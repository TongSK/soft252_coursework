/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import patientmanagementsystem.adminModel.Admin;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.model.Medicine;
import patientmanagementsystem.model.Seriliaser;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author shekei
 */
public class LoadData {
    
    public LoadData()
    {
    
    }
    
    //Create JSON files if system needs to be reset
    public void loadUsers()
    {
        String first = "Sarah";
        String last = "Sar";
        String userId = "A1234";
        String password = "password";
        String address = "address";
        String postCode = "pl1 123";
        
        Admin testAdmin = new Admin(first, last, userId, password, address, postCode);
        
        first = "Suzi";
        last = "Suz";
        userId = "D1234";
        password = "password";
        address = "another address";
        postCode = "PL4 123";
        
        Doctor testDoc = new Doctor(first, last, userId, password, address, postCode);
        
        first = "Sammy";
        last = "Sam";
        userId = "D1235";
        password = "password";
        address = "another address";
        postCode = "PL4 123";
        
        Doctor twoDoc = new Doctor(first, last, userId, password, address, postCode);
        
        first = "Sally";
        last = "Sal";
        userId = "D1236";
        password = "password";
        address = "another address";
        postCode = "PL4 123";
        
        Doctor thirdDoc = new Doctor(first, last, userId, password, address, postCode);
        
        first = "Bobby";
        last = "Bob";
        userId = "P1234";
        password = "password";
        address = "an address";
        postCode = "PL5 123";
        String gender = "Male";
        LocalDate birthday = LocalDate.of(1999, Month.OCTOBER, 12);
        
        Patient testPatient = new Patient(first, last, userId, password, address, postCode, gender, birthday);
        
        first = "Billie";
        last = "Bill";
        userId = "P1235";
        password = "password";
        address = "an address";
        postCode = "PL9 123";
        gender = "Female";
        birthday = LocalDate.of(1997, Month.OCTOBER, 13);
        
        Patient twoPatient = new Patient(first, last, userId, password, address, postCode, gender, birthday);
        
        first = "Lillie";
        last = "Lill";
        userId = "P1236";
        password = "password";
        address = "an address";
        postCode = "PL3 123";
        gender = "Female";
        birthday = LocalDate.of(1997, Month.APRIL, 13);
        
        
        Patient thirdPatient = new Patient(first, last, userId, password, address, postCode, gender, birthday);
        
        first = "David";
        last = "Dav";
        userId = "S1234";
        password = "password";
        address = "address";
        postCode = "PL1 123";
        
        Secretary testSecretary = new Secretary(first, last, userId, password, address, postCode);
        
        Seriliaser patientSer = new Seriliaser("patients.ser");
        Seriliaser doctorSer = new Seriliaser("doctors.ser");
        Seriliaser secretarySer = new Seriliaser("secretary.ser");
        Seriliaser adminSer = new Seriliaser("admins.ser");
        
        ArrayList<Patient> allPatients = new ArrayList<Patient>();
        allPatients.add(testPatient);
        allPatients.add(twoPatient);
        allPatients.add(thirdPatient);
        ArrayList<Doctor> allDoctors = new ArrayList<Doctor>();
        allDoctors.add(testDoc);
        allDoctors.add(twoDoc);
        allDoctors.add(thirdDoc);
        ArrayList<Secretary> allSecretaries = new ArrayList<Secretary>();
        allSecretaries.add(testSecretary);
        ArrayList<Admin> allAdmins = new ArrayList<Admin>();
        allAdmins.add(testAdmin);
        
        patientSer.writeObject(allPatients); //Write to json files
        doctorSer.writeObject(allDoctors);
        secretarySer.writeObject(allSecretaries);
        adminSer.writeObject(allAdmins);
        
        Seriliaser medSer = new Seriliaser("medicine.ser");
        ArrayList<Medicine> allMeds = new ArrayList<Medicine>();
        
        String medName = "Salt";
        Medicine testMedicine = new Medicine(medName);
        
        medName = "Peroxide";
        testMedicine = new Medicine(medName);
        allMeds.add(testMedicine);
        
        medName = "Sodium Chloride";
        testMedicine = new Medicine(medName);
        allMeds.add(testMedicine);
        
        medName = "Hydrochloric Acid";
        testMedicine = new Medicine(medName);
        allMeds.add(testMedicine);
        
        medName = "Benzonoate";
        testMedicine = new Medicine(medName);
        allMeds.add(testMedicine);
        
        medName = "Potassium Nitrate";
        testMedicine = new Medicine(medName);
        allMeds.add(testMedicine);
        
        medName = "Deoxynucleic Acid";
        testMedicine = new Medicine(medName);
        allMeds.add(testMedicine);
        
        medName = "Methyl Ethonoate";
        testMedicine = new Medicine(medName);
        allMeds.add(testMedicine);
        
        medName = "Ribose";
        testMedicine = new Medicine(medName);
        allMeds.add(testMedicine);
        
        medName = "Calcium Carbonate";
        testMedicine = new Medicine(medName);
        allMeds.add(testMedicine);
        
        medSer.writeObject(allMeds);
        
        System.out.println("SUCCESSFUL WRITING");
    
    }
    
}
