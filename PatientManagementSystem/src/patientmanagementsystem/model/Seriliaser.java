package patientmanagementsystem.model;


import java.io.*;

/**
 *
 * @author shekei
 */
public class Seriliaser {

    private String fileName;
    
    public Seriliaser(String theFileName)
    {
        this.fileName = theFileName;
    }

    /**
     *Load objects from JSON file
     * @return object i.e arrayLists of each user subclass and medicine
     */
    public Serializable readObject() {
        
        Serializable theObject = null;
        String message = "";
        
        try {
         FileInputStream file = new FileInputStream(fileName); //Open reading stream
         ObjectInputStream inputStream = new ObjectInputStream(file);
         
         theObject = (Serializable) inputStream.readObject();
         
         inputStream.close();
         file.close(); //Close reading stream
         
         
        } catch (IOException i) {
            
            message = this.fileName + " FILE NOT FOUND";
            System.out.println(message);
            
        } catch (ClassNotFoundException c) {
            message = this.fileName + " CLASS NOT FOUND";
            System.out.println(message);
        }
        
        return theObject;
        
        
    }

    /**
     * Save objects to JSON file
     * @param object
     */
    public void writeObject(Serializable object) {
        
        String message = "";
        
        try {
            FileOutputStream file = new FileOutputStream(fileName);
            ObjectOutputStream outputStream = new ObjectOutputStream(file);

            outputStream.writeObject(object);
            
            outputStream.close();
            file.close();
            
            
         } catch (IOException i) {
             
            message = this.fileName + " FAILED TO LOAD";
            System.out.println(message);
            
         }
    }
}
