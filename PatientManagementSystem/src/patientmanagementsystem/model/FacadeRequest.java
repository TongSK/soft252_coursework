package patientmanagementsystem.model;


import java.io.Serializable;
import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.User;


/**
 *
 * @author sheke
 */
public class FacadeRequest implements Serializable {
    
    private static final long serialVersionUID = 4L;
    private User sender;
    private User receiver;
    
    /**
     *
     * @param theSender
     * @param theReceiver
     */
    public FacadeRequest(User theSender, User theReceiver) {
        this.sender = theSender;
        this.receiver = theReceiver;
    }
      
    /**
     *
     * @param theRequest
     * @param notification
     */
    public void sendFacadeRequest(Request theRequest, String notification) {
        receiver.receiveRequest(theRequest);
        receiver.notify(notification);
    }
}
