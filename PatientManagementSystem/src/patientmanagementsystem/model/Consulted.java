package patientmanagementsystem.model;


import java.io.Serializable;
import patientmanagementsystem.patientModel.FeedbackRequest;
import patientmanagementsystem.doctorModel.Doctor;


/**
 *
 * @author sheke
 */
public class Consulted implements IState, Serializable {
    
    private static final long serialVersionUID = 3L;

    /**
     *
     * @param doctor
     * @param rating
     * @param comment
     */
    @Override
    public Request giveFeedback(Doctor doctor, String rating, String comment) {
        FeedbackRequest feedback = new FeedbackRequest(doctor, rating, comment);
        return feedback;
    }
    
    
}
