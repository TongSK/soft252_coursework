package patientmanagementsystem.model;


import patientmanagementsystem.model.IObserver;


/**
 *
 * @author sheke
 */
public interface IObservable{

    /**
     *
     */
    public void notifyObservers();

    /**
     *
     * @param patient
     */
    public void removeObserver(IObserver patient);

    /**
     *
     * @param patient
     */
    public void addObserver(IObserver patient);
}
