package patientmanagementsystem.model;


import java.io.Serializable;
import patientmanagementsystem.model.User;
import java.util.ArrayList;

/**
 *
 * @author sheke
 */
public class TemplateRequest implements IRequest, Serializable {
    private static final long serialVersionUID = 5L;

    /**
     *
     */
    protected ArrayList<String> allNotifications;

    /**
     *
     */
    protected ArrayList<Request> allRequests;

    /**
     *
     * @param theRequest
     * @param receiver
     * @param sender
     * @param notification
     */
    @Override
    public void sendRequest(Request theRequest, User receiver,User sender, String notification) {
        
        FacadeRequest facadeRequest = new FacadeRequest(sender, receiver);
        facadeRequest.sendFacadeRequest(theRequest, notification);
    }

    /**
     *
     * @param theRequest
     */
    @Override
    public void receiveRequest(Request theRequest) {
        this.allRequests.add(theRequest);
    }

    /**
     *
     * @param notification
     */
    @Override
    public void notify(String notification) {
        this.allNotifications.add(notification);
    }

    /**
     *
     * @return
     */
    public ArrayList<String> getAllNotifications() {
        return allNotifications;
    }

    /**
     *
     * @return
     */
    public ArrayList<Request> getAllRequests() {
        return allRequests;
    }

    public void setAllNotifications(ArrayList<String> allNotifications) {
        this.allNotifications = allNotifications;
    }

    public void setAllRequests(ArrayList<Request> allRequests) {
        this.allRequests = allRequests;
    }
    
    

    
    
}
