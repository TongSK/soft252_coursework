package patientmanagementsystem.model;

import java.io.Serializable;


/**
 *
 * @author sheke
 */
public class Request implements Serializable {
    
    private static final long serialVersionUID = 7L;

    /**
     *
     */
    protected String requestType;

    /**
     *
     * @return
     */
    public String getRequestType() {
        return requestType;
    }
    
}
