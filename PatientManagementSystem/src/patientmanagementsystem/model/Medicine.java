package patientmanagementsystem.model;


import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author sheke
 */
public class Medicine implements IObservable, Serializable {
    
    private static final long serialVersionUID = 5678L;
    private String name;
    private int stock;
    private ArrayList<IObserver> allObservers;

    /**
     *
     * @param theName of the medicine
     */
    public Medicine(String theName) {
        this.name = theName;
        this.stock = 0; //Secretary will change the stock
        
        ArrayList<IObserver> observers = new ArrayList<IObserver>();
        this.allObservers = observers;
    }

    /**
     *
     */
    @Override
    public void notifyObservers() {
        if(this.allObservers != null && this.allObservers.size() > 0)
        {
            for(IObserver observer : this.allObservers)
            {
                observer.update();
            }
        }
    }

    /**
     * 
     * @param patient the object we need to remove from arrayList
     * Find the index of it and then remove
     */
    @Override
    public void removeObserver(IObserver patient) {
        
        if(this.allObservers != null && this.allObservers.size() > 0)
        {
            for (int i = 0; i < this.allObservers.size(); i++) {
                if(allObservers.get(i) == patient)
                {
                    allObservers.remove(i);
                    break;
                }
            }
        }
        
    }

    /**
     * 
     * @param patient
     */
    @Override
    public void addObserver(IObserver patient) {
        
        if(this.allObservers != null && this.allObservers.size() > 0)
        {
            for (int i = 0; i < this.allObservers.size(); i++) {
                if(allObservers.get(i) == patient)
                    {
                        allObservers.remove(i);
                        this.allObservers.add(patient);
                        break;
                    }   
            }
        }
        else
        {
            this.allObservers.add(patient);
        }
    }
    
    //GETTERS AND SETTERS
    
    public void setStock(int amount) {
        
        if(amount > 0) //If amount is positive - i.e. restocking
        {
            notifyObservers();
        }
        
        this.stock = this.stock + amount;
    }
    
    public String getName() {
        return name;
    }

  
    public void setName(String name) {
        this.name = name;
    }

    public int getStock() {
        return stock;
    }

    
    
    
    
    
}
