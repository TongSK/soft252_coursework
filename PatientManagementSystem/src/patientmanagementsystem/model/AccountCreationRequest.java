package patientmanagementsystem.model;


import java.time.LocalDate;

/**
 *
 * @author sheke
 */
public class AccountCreationRequest extends Request {

    private String forename;

    private String surname;

    private String address;

    private String postCode;

    private String gender;

    private LocalDate dateOfBirth;

    /**
     *
     * @param firstName
     * @param lastName
     * @param theAddress
     * @param thePostCode
     * @param theGender
     * @param dob
     */
    public AccountCreationRequest(String firstName, String lastName, String theAddress, String thePostCode, String theGender, LocalDate dob) {
        
        this.requestType = "CREATE ACCOUNT REQUEST";
        
        this.forename = firstName;
        this.surname = lastName;
        this.address = theAddress;
        this.postCode = thePostCode;
        this.gender = theGender;
        this.dateOfBirth = dob;
    }

    /**
     *
     * @return
     */
    public String getForename() {
        return forename;
    }

    /**
     *
     * @param forename
     */
    public void setForename(String forename) {
        this.forename = forename;
    }

    /**
     *
     * @return
     */
    public String getSurname() {
        return surname;
    }

    /**
     *
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     *
     * @param postCode
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    /**
     *
     * @return
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     *
     * @param dateOfBirth
     */
    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    
    
}
