package patientmanagementsystem.model;


import java.io.Serializable;
import patientmanagementsystem.doctorModel.Prescription;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.doctorModel.Doctor;
import java.util.ArrayList;
import patientmanagementsystem.patientModel.FeedbackRequest;

/**
 *
 * @author shekei
 */
public class Appointment implements Serializable {
    
    private static final long serialVersionUID = 6L;
 
    private Doctor doctor;
    private Patient patient;
    private IState appointmentStatus;
    private String dateTime;
    private String consultationNotes;
    private ArrayList<Prescription> prescribedMedicine;
    private String prescriptionStatus;

    /**
     *
     * @param doctor
     * @param patient
     * @param dateTime
     */
    public Appointment(Doctor doctor, Patient patient, String dateTime) {
        this.doctor = doctor;
        this.patient = patient;
        this.dateTime = dateTime;
        
        Awaiting awaiting = new Awaiting();
        this.appointmentStatus = awaiting;
        
        this.consultationNotes = "";
        
        ArrayList<Prescription> prescribedMedicine = new ArrayList<Prescription>();
        this.prescribedMedicine = prescribedMedicine;
        
        this.prescriptionStatus = "NO PRESCRIPTION";
    }
    
    public FeedbackRequest allowFeedback(Doctor theDoctor, String theRating, String theComment)
    {
        FeedbackRequest theRequest = (FeedbackRequest)this.appointmentStatus.giveFeedback(theDoctor, theRating, theComment);
        return theRequest;
    }
    
    //GETTERS AND SETTERS

    /**
     *
     * @return
     */

    public Doctor getDoctor() {
        return doctor;
    }

    /**
     *
     * @param doctor
     */
    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    /**
     *
     * @return
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     *
     * @param patient
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    /**
     *
     * @return
     */
    public IState getAppointmentStatus() {
        return appointmentStatus;
    }

    /**
     *
     * @param appointmentStatus
     */
    public void setAppointmentStatus(IState appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    /**
     *
     * @return
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     *
     * @param dateTime
     */
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    /**
     *
     * @return
     */
    public String getConsultationNotes() {
        return consultationNotes;
    }

    /**
     *
     * @param consultationNotes
     */
    public void setConsultationNotes(String consultationNotes) {
        this.consultationNotes = consultationNotes;
    }

    /**
     *
     * @return
     */
    public ArrayList<Prescription> getPrescribedMedicine() {
        return prescribedMedicine;
    }

    /**
     *
     * @param prescribedMedicine
     */
    public void setPrescribedMedicine(ArrayList<Prescription> prescribedMedicine) {
        this.prescribedMedicine = prescribedMedicine;
    }

    /**
     *
     * @return
     */
    public String getPrescriptionStatus() {
        return prescriptionStatus;
    }

    /**
     *
     * @param prescriptionStatus
     */
    public void setPrescriptionStatus(String prescriptionStatus) {
        this.prescriptionStatus = prescriptionStatus;
    }
    
    
    
    
    
}
