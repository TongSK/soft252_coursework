package patientmanagementsystem.model;


import java.io.Serializable;
import patientmanagementsystem.secretaryModel.Secretary;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.adminModel.Admin;
import java.util.ArrayList;

/**
 *
 * @author shekei
 */
public class Singleton implements Serializable {
    private static final long serialVersionUID = 123456789L;
    private ArrayList<User> patients;
    private ArrayList<User> doctors;
    private ArrayList<User> secretary;
    private ArrayList<User> admin;
    private ArrayList<Medicine> medicine;
    
    private Seriliaser patientFile;
    private Seriliaser adminFile;
    private Seriliaser doctorFile;
    private Seriliaser secretaryFile;
    private Seriliaser medicineFile;

    private static Singleton uniqueInstance = null;
    
    private Singleton()
    {
        this.patients = new ArrayList<User>();
        this.doctors = new ArrayList<User>();
        this.secretary = new ArrayList<User>();
        this.admin = new ArrayList<User>();
        this.medicine = new ArrayList<Medicine>();
        
        String patientName = "patients.ser";
        this.patientFile = new Seriliaser(patientName);
        loadPatientArray(this.patientFile);
        
        String adminName = "admins.ser";
        this.adminFile = new Seriliaser(adminName);
        loadAdminArray(this.adminFile);
        
        String doctorName = "doctors.ser";
        this.doctorFile = new Seriliaser(doctorName);
        loadDoctorArray(this.doctorFile);
        
        String secretaryName = "secretary.ser";
        this.secretaryFile = new Seriliaser(secretaryName);
        loadSecretaryArray(this.secretaryFile);
        
        String medicineName = "medicine.ser";
        this.medicineFile = new Seriliaser(medicineName);
        loadMedicineArray(this.medicineFile);
    }

    
    public static Singleton getInstance() {
        
        if(uniqueInstance == null) //Create only one single instance of object
        {
            uniqueInstance = new Singleton();
            System.out.println("New instance");
        }
        else
        {
            System.out.println("Existing instance");
        }
        
        return uniqueInstance;
    }
    
    //load_______ methods return array of varType from respective JSON files and set as class attributes
    
    public void loadPatientArray(Seriliaser thePatients) {
        
        this.patients = (ArrayList<User>)thePatients.readObject();
    }

    public void loadDoctorArray(Seriliaser theDoctors) {
        this.doctors = (ArrayList<User>)theDoctors.readObject();
    }

    public void loadSecretaryArray(Seriliaser theSecretaries) {
        this.secretary = (ArrayList<User>)theSecretaries.readObject();
    }

    public void loadAdminArray(Seriliaser theAdmins) {
        this.admin = (ArrayList<User>)theAdmins.readObject();
    }

    public void loadMedicineArray(Seriliaser theMedicines) {
        this.medicine = (ArrayList<Medicine>)theMedicines.readObject();
    }

    //save______ methods write arrayLists of each object type into respective JSON files
    
    public void saveMedicineArray(Seriliaser theMedicines) {
        theMedicines.writeObject(this.medicine);
    }

    public void savePatientArray(Seriliaser thePatients) {
        thePatients.writeObject(this.patients);
    }

    public void saveDoctorArray(Seriliaser theDoctors) {
        theDoctors.writeObject(this.doctors);
    }

    public void saveSecretaryArray(Seriliaser theSecretaries) {
        theSecretaries.writeObject(this.secretary);
    }

    public void saveAdminArray(Seriliaser theAdmins) {
        theAdmins.writeObject(this.admin);
    }
    
    public void saveAll()
    {
        saveMedicineArray(this.medicineFile);
        savePatientArray(this.patientFile);
        saveDoctorArray(this.doctorFile);
        saveSecretaryArray(this.secretaryFile);
        saveAdminArray(this.adminFile);
        
        System.out.println("All save successful");
    
    }
    
    public void saveUser(User user)
    {
        char userType = user.getUserId().charAt(0); //Get user type from first char id
        
        switch(userType)
        {
            case 'A':
                removeOldUser(this.admin, user);
                break;
            case 'D':
                removeOldUser(this.doctors, user);
                break;
            case 'S':
                removeOldUser(this.secretary, user);
                break;
            case 'P':
                removeOldUser(this.patients, user);
                break;
        }
        
    }
    
    public void removeOldUser(ArrayList<User> theList, User newUser)
    {
        for (int i = 0; i < theList.size(); i++) {
            if(newUser.getUserId().equals(theList.get(i).getUserId())) //Look for user in list through id
            {
                theList.add(newUser); //Add user with the new changes
                theList.remove(i); //Remove the same user but without the new changes
                System.out.println("New user saved");
                break;
            }
        }
    }
    
    //GETTERS AND SETTERS for ArrayLists

    public ArrayList<User> getPatients() {
        return patients;
    }

    public void setPatients(ArrayList<User> patients) {
        this.patients = patients;
    }

    public ArrayList<User> getDoctors() {
        return doctors;
    }

    public void setDoctors(ArrayList<User> doctors) {
        this.doctors = doctors;
    }

    public ArrayList<User> getSecretary() {
        return secretary;
    }

    public void setSecretary(ArrayList<User> secretary) {
        this.secretary = secretary;
    }

    public ArrayList<User> getAdmin() {
        return admin;
    }

    public void setAdmin(ArrayList<User> admin) {
        this.admin = admin;
    }

    public ArrayList<Medicine> getMedicine() {
        return medicine;
    }

    public void setMedicine(ArrayList<Medicine> medicine) {
        this.medicine = medicine;
    }
    
}
