package patientmanagementsystem.model;


import patientmanagementsystem.patientModel.AppointmentRequest;
import patientmanagementsystem.secretaryModel.Secretary;


/**
 *
 * @author sheke
 */
public interface IRespond {

    /**
     *
     * @param theRequest
     * @param response
     * @return
     */
    public void setResponse(AppointmentRequest theRequest, String response, Secretary secretary);
}
