package patientmanagementsystem.model;


import java.io.Serializable;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.patientModel.FeedbackRequest;


/**
 *
 * @author sheke
 */
public class Cancelled implements IState, Serializable {
    
    private static final long serialVersionUID = 2L;

    /**
     *
     * @param doctor
     * @param rating
     * @param comment
     */
    @Override
    public Request giveFeedback(Doctor doctor, String rating, String comment) {
        FeedbackRequest request = null;
        return request;
    }
    
    
}
