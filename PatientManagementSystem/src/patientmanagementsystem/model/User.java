package patientmanagementsystem.model;
import java.io.Serializable;



/**
 *
 * @author shekei
 */
public class User extends TemplateRequest implements Serializable {
    private static final long serialVersionUID = 1234L;
    protected String firstName;
    protected String lastName;
    protected String userId;
    protected String password;
    protected String address;
    protected String postCode;

    /**
     *
     * @param index of the ArrayList containing notification messages
     */
    public void removeNotification(int index) {
        
        this.allNotifications.remove(index);
    }

    /**
     *
     * @param index of the ArrayList containing Request objects
     */
    public void removeRequest(int index) {
        this.allRequests.remove(index);
    }
    
    //GETTERS AND SETTERS

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
    

}
