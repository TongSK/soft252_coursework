package patientmanagementsystem.model;


import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.User;


/**
 *
 * @author sheke
 */
public interface IRequest {

    /**
     *
     * @param theRequest
     * @param receiver
     * @param sender
     * @param notification
     */
    public void sendRequest(Request theRequest, User receiver, User sender, String notification);

    /**
     *
     * @param theRequest
     */
    public void receiveRequest(Request theRequest);

    /**
     *
     * @param notification
     */
    public void notify(String notification);
}
