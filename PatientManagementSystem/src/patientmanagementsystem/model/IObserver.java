package patientmanagementsystem.model;


/**
 *
 * @author sheke
 */
public interface IObserver {

    /**
     *
     */
    public void update();
}
