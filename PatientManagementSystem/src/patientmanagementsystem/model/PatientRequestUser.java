package patientmanagementsystem.model;


import patientmanagementsystem.model.AccountCreationRequest;
import patientmanagementsystem.secretaryModel.Secretary;
import java.time.LocalDate;

/**
 *
 * @author shekei
 */
public class PatientRequestUser {

    private AccountCreationRequest theRequest;
    
    public PatientRequestUser(AccountCreationRequest request)
    {
        this.theRequest = request;
    }

    /**
     *
     * @param theSecretary who will receive the request and be in charge of approving request
     */
    public Secretary SendRequest(Secretary theSecretary) {
        
        String notification = LocalDate.now().toString() + " - POTENTIAL PATIENT REQUESTS ACCOUNT";
        theSecretary.receiveRequest(this.theRequest);
        theSecretary.notify(notification);
        
        return theSecretary;
        
    }
}
