package patientmanagementsystem.model;


import patientmanagementsystem.doctorModel.Doctor;


/**
 *
 * @author sheke
 */
public interface IState {

    /**
     *
     * @param doctor
     * @param rating
     * @param comment
     */
    public Request giveFeedback(Doctor doctor, String rating, String comment);
}
