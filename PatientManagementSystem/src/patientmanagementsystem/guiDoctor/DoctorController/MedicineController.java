/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiDoctor.DoctorController;

import java.util.ArrayList;
import patientmanagementsystem.Controller.GetUserController;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.guiDoctor.RequestMedicine;
import patientmanagementsystem.model.Medicine;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author shekei
 */
public class MedicineController {
    
    private Singleton singleton = Singleton.getInstance();
    private GetUserController controller = new GetUserController();
    
    public MedicineController()
    {
    
    }
    
    public void toRequestMedicine(Doctor doctor)
    {
        RequestMedicine toPage = new RequestMedicine(doctor);
        toPage.setVisible(true);
    }
    
    public void createMedicine(Doctor doctor, String medicineName)
    {
        Secretary secretary = controller.getRandomSecretary(); //Get random secretary to  be notified
        Medicine newMedicine = doctor.createMedicine(medicineName, secretary);
        ArrayList<Medicine> currentMedicines = singleton.getMedicine();
        currentMedicines.add(newMedicine); //Add new medicine onto arrayList
        singleton.setMedicine(currentMedicines); //Set back into singleton
        singleton.saveAll(); //Save back into json
    }
    
    public void goBack(RequestMedicine thisPage)
    {
        thisPage.dispose();
    }
    
}
