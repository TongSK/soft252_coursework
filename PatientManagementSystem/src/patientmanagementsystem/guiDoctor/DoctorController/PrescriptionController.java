/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiDoctor.DoctorController;

import java.util.ArrayList;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.doctorModel.Prescription;
import patientmanagementsystem.guiDoctor.Consultation;
import patientmanagementsystem.guiDoctor.PrescriptionPage;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Medicine;

/**
 *
 * @author shekei
 */
public class PrescriptionController {
    
    public PrescriptionController()
    {
    
    }
    
    //Create new prescription object and add to arrayList inside appointment object
    public void createPrescriptionObject(Medicine medicine, int quantity, String dosage, PrescriptionPage currentPage, Appointment appointment, Doctor doctor)
    {
        appointment = doctor.createPrescription(appointment, medicine, quantity, dosage);
        backToConsultation(appointment, doctor,currentPage);//Go back to consultation page
        
    }
    
    public void backToConsultation(Appointment appointment, Doctor doctor, PrescriptionPage currentPage)
    {
        Consultation consultationPage = new Consultation(doctor, appointment); 
        consultationPage.setVisible(true);
        currentPage.dispose();
    }
    
}
