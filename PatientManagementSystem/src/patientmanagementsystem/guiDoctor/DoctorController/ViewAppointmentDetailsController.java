/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiDoctor.DoctorController;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.doctorModel.Prescription;
import patientmanagementsystem.guiDoctor.ViewAppointmentDetails;
import patientmanagementsystem.guiDoctor.ViewPatients;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.patientModel.Patient;

/**
 *
 * @author shekei
 */
public class ViewAppointmentDetailsController {
    
    public ViewAppointmentDetailsController()
    {
    
    }
    
    public void toViewDetails(Appointment appointment, Doctor doctor)
    {
        ViewAppointmentDetails page = new ViewAppointmentDetails( appointment, doctor);
        page.setVisible(true);
    }
    
    //returns paragraph of prescribed medicine and details
    public String outputPrescriptionDetails(Appointment appointment)
    {
        String outputText = "";
        String prescriptionStatus = "Appointment Prescription Status: "+appointment.getPrescriptionStatus() + "\n\n";
        
        outputText = outputText + prescriptionStatus;
        
        ArrayList<Prescription> allPrescriptions = appointment.getPrescribedMedicine();
        
        for (int i = 0; i < allPrescriptions.size(); i++) {
            
            Prescription prescription = allPrescriptions.get(i); //For each prescription in prescription arrayList
            String medicine = "Medicine: " + prescription.getMedicine().getName() + "\n";
            String quantity = "Quantity: " + prescription.getQuantity() + "\n";
            String dosage = "Dosage: " + prescription.getDosage() + "\n";
            
            String received = "Received: ";
            
            if(prescription.getReceived() == true)
            {
                received = received + "YES" + "\n";
            }
            else
            {
                received = received + "NO" + "\n";
            }
            
            String prescriptionMed = medicine + quantity + dosage + received;
            outputText = outputText + prescriptionMed;
            
        }
        
        return outputText;
    }
    
    //Return with paragraph of patient's details
    public String outputPatientDetails(Appointment appointment)
    {
        String outputText="";
        Patient patient = appointment.getPatient();
        
        LocalDate now = LocalDate.now();
        LocalDate birthdate = appointment.getPatient().getDateOfBirth();
        Period period = Period.between(birthdate, now); //Calculate duration between now and birth date
        
        String patientId = "Patient ID: " + patient.getUserId() + "\n";
        String patientName = "Patient Name: " + patient.getFirstName() + " " + patient.getLastName() + "\n";
        String patientAddress = "Patient Address: " + patient.getAddress() + " , " + patient.getPostCode() + "\n";
        String patientAge = "Patient's Age: " + String.valueOf(period.getYears()) + "\n";
        
        outputText = patientId + patientName + patientAddress + patientAge;
        
        return outputText;
        
    }
    
    //Return with paragraph of doctor's details
    public String outputDoctorDetails(Appointment appointment)
    {
        String outputText="";
        Doctor doctor = appointment.getDoctor();
        
        String doctorId = "Doctor ID: " + doctor.getUserId() + "\n";
        String doctorName = "Doctor Name: " + doctor.getFirstName() + " " + doctor.getLastName() + "\n";
        String doctorAddress = "Doctor Address: " + doctor.getAddress() + " , " + doctor.getPostCode() + "\n";
        
        outputText = doctorId + doctorName + doctorAddress;
        
        return outputText;
    }
    
    public void backToPatients(ViewAppointmentDetails thisPage)
    {
        thisPage.dispose();
    }
    
}
