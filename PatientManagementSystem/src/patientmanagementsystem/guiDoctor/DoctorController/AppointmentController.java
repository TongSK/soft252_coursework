/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiDoctor.DoctorController;

import java.util.ArrayList;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.guiDoctor.Consultation;
import patientmanagementsystem.guiDoctor.ViewAppointments;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Awaiting;
import patientmanagementsystem.model.IState;
import patientmanagementsystem.model.User;
import patientmanagementsystem.patientModel.Patient;

/**
 *
 * @author shekei
 */
public class AppointmentController {
    
    public AppointmentController()
    {
    }
    
    //For each patient get their arrayList of Appointment objects
    //Pass into another method to filter the relevant appointment objects
    public ArrayList<Appointment> getPatientAppointments(ArrayList<User> allPatients, Doctor doctor)
    {
        ArrayList<Appointment> doctorAppointments = new ArrayList<Appointment>();
        
        for (int i = 0; i < allPatients.size(); i++) {
            
            Patient thePatient = (Patient) allPatients.get(i);
            ArrayList<Appointment> allAppointments = thePatient.getAllAppointments();
            doctorAppointments = getRelevantAppointments(allAppointments, doctor);
            
        }
        
        return doctorAppointments;
        
    }
    
    //Get appointments that involve one doctor and have yet to have an consulting
    public ArrayList<Appointment> getRelevantAppointments(ArrayList<Appointment> allAppointments, Doctor doctor)
    {
        ArrayList<Appointment> relevantAppointments = new ArrayList<Appointment>();
        String doctorId = doctor.getUserId(); //Get id of doctor to filter results
        Awaiting awaitingStatus = new Awaiting(); //Yet to have consultation status
        
        for (int i = 0; i < allAppointments.size(); i++) {
            
            Appointment checkAppointment = allAppointments.get(i);
            IState appointmentStatus = checkAppointment.getAppointmentStatus();
            String checkDoctor = checkAppointment.getDoctor().getUserId();
            
            if(doctorId.equals(checkDoctor)== true && appointmentStatus.getClass().equals(awaitingStatus.getClass()) == true)
            {
                relevantAppointments.add(checkAppointment);
            }    
            
        }
        
        return relevantAppointments;
    }
    
    //Pass appointment object into consultation page and make page visible
    public boolean passAppointment(Appointment appointment, ViewAppointments currentPage)
    {
        boolean success = true;
        Consultation consultationPage = new Consultation(appointment.getDoctor(), appointment);
        consultationPage.setVisible(true);
        currentPage.dispose();
        return success;
    }
    
    
    
}
