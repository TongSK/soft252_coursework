/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiDoctor.DoctorController;

import java.time.LocalDate;
import java.util.ArrayList;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.doctorModel.Prescription;
import patientmanagementsystem.guiDoctor.Consultation;
import patientmanagementsystem.guiDoctor.DoctorHomePage;
import patientmanagementsystem.guiDoctor.PrescriptionPage;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.patientModel.Patient;

/**
 *
 * @author shekei
 */
public class ConsultationController {
    
    private Singleton singleton = Singleton.getInstance();
    public ConsultationController()
    {
    
    }
    
    //Go to prescription page
    public boolean toPrescriptionPage(Consultation thisPage, Doctor doctor, Appointment appointment)
    {
        PrescriptionPage nextPage = new PrescriptionPage(doctor, appointment);
        nextPage.setVisible(true);
        thisPage.dispose();
        
        return true;
    }
    
    public Appointment removePrescription(Appointment appointment, int index)
    {
        ArrayList<Prescription> currentPrescription = appointment.getPrescribedMedicine(); //Get list of current prescribed med
        currentPrescription.remove(index); //Remove specified object
        appointment.setPrescribedMedicine(currentPrescription); //Save back to appointment object
        
        return appointment;
        
    }
    
    public void backToHomePage(Consultation thisPage, Doctor doctor)
    {
        DoctorHomePage home = new DoctorHomePage(doctor);
        home.setVisible(true);
        thisPage.dispose();
    }
    
    public void finishConsultation(Appointment appointment, String notes, Doctor doctor, Consultation currentPage)
    {
        Patient patient = appointment.getPatient();
        patient = doctor.finishAppointment(appointment, notes); //Add notes and change IState to Consulted
        singleton.saveUser(patient);//Save user back to json
        backToHomePage(currentPage, doctor);
        
    }
    
    
    
}
