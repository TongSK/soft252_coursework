/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiDoctor.DoctorController;

import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.guiDoctor.ViewComment;

/**
 *
 * @author shekei
 */
public class ViewFeedbackController {
    
    public ViewFeedbackController()
    {
    
    }
    
    public void toViewComment(Doctor theDoctor, int feedbackIndex)
    {
        ViewComment openPage = new ViewComment(theDoctor, feedbackIndex);
        openPage.setVisible(true);
    }
    
    public void closeComment(ViewComment thisPage)
    {
        thisPage.dispose();
    }
    
}
