/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiDoctor;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import patientmanagementsystem.Controller.NotificationsController;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.guiDoctor.DoctorController.MedicineController;
import patientmanagementsystem.theHomePage;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.model.User;

/**
 *
 * @author shekei
 */
public class DoctorHomePage extends javax.swing.JFrame {
    
    private Doctor theDoctor;
    private Singleton singleton = Singleton.getInstance();
    private MedicineController controller = new MedicineController();
    private NotificationsController notificationController = new NotificationsController();

    /**
     * Creates new form DoctorHomePage
     */
    public DoctorHomePage(User userObj) {
        this.theDoctor = (Doctor) userObj;
        initComponents();
        getNotifications();
    }
    
    public void toHomePage()
    {
        theHomePage home = new theHomePage();
        home.setVisible(true);
        this.dispose();
        
    }
    
    public void getNotifications()
    {
        ArrayList<String> notifications = this.theDoctor.getAllNotifications();
        
        if(notifications != null ) 
        {
            DefaultListModel model = new DefaultListModel(); //Create a model to add values to
            
            for (int i = 0; i < notifications.size(); i++) {
                
                String message = notifications.get(i);
                model.addElement(message);
                lstNotification.setModel(model); //Pass model of all names to the JList component so it is visible
            }
        }
    }
    
    public void toViewRequest()
    {
        ViewAppointmentRequests viewPage = new ViewAppointmentRequests(this.theDoctor);
        viewPage.setVisible(true);
        this.dispose();
    }
    
    public void toViewFeedback()
    {
        ViewOwnFeedback page = new ViewOwnFeedback(this.theDoctor);
        page.setVisible(true);
        this.dispose();
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstNotification = new javax.swing.JList<>();
        lblNotif = new javax.swing.JLabel();
        btnRemoveNotif = new javax.swing.JButton();
        lblWelcome = new javax.swing.JLabel();
        btnRequestMed = new javax.swing.JButton();
        btnViewRequest = new javax.swing.JButton();
        btnViewAppointment = new javax.swing.JButton();
        btnViewHistory = new javax.swing.JButton();
        btnLogOut = new javax.swing.JButton();
        btnViewFeedback = new javax.swing.JButton();
        btnPassword = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 204));

        lstNotification.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jScrollPane1.setViewportView(lstNotification);

        lblNotif.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        lblNotif.setForeground(new java.awt.Color(0, 0, 0));
        lblNotif.setText("Notifications");

        btnRemoveNotif.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnRemoveNotif.setForeground(new java.awt.Color(0, 0, 0));
        btnRemoveNotif.setText("Remove Notification");
        btnRemoveNotif.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveNotifActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(341, 341, 341)
                .addComponent(lblNotif)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 760, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(btnRemoveNotif)
                        .addGap(46, 46, 46))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblNotif)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 614, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addComponent(btnRemoveNotif)
                .addGap(24, 24, 24))
        );

        lblWelcome.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        lblWelcome.setForeground(new java.awt.Color(0, 0, 0));
        lblWelcome.setText("Welcome DOCTOR");

        btnRequestMed.setBackground(new java.awt.Color(255, 255, 204));
        btnRequestMed.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        btnRequestMed.setForeground(new java.awt.Color(0, 0, 0));
        btnRequestMed.setText("Request Medicine Order");
        btnRequestMed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRequestMedActionPerformed(evt);
            }
        });

        btnViewRequest.setBackground(new java.awt.Color(255, 255, 204));
        btnViewRequest.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        btnViewRequest.setForeground(new java.awt.Color(0, 0, 0));
        btnViewRequest.setText("View Appointment Requests");
        btnViewRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewRequestActionPerformed(evt);
            }
        });

        btnViewAppointment.setBackground(new java.awt.Color(255, 255, 204));
        btnViewAppointment.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        btnViewAppointment.setForeground(new java.awt.Color(0, 0, 0));
        btnViewAppointment.setText("View Appointments");
        btnViewAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewAppointmentActionPerformed(evt);
            }
        });

        btnViewHistory.setBackground(new java.awt.Color(255, 255, 204));
        btnViewHistory.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        btnViewHistory.setForeground(new java.awt.Color(0, 0, 0));
        btnViewHistory.setText("View Patient History");
        btnViewHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewHistoryActionPerformed(evt);
            }
        });

        btnLogOut.setBackground(new java.awt.Color(255, 255, 204));
        btnLogOut.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        btnLogOut.setForeground(new java.awt.Color(0, 0, 0));
        btnLogOut.setText("Log Out");
        btnLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogOutActionPerformed(evt);
            }
        });

        btnViewFeedback.setBackground(new java.awt.Color(255, 255, 204));
        btnViewFeedback.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        btnViewFeedback.setForeground(new java.awt.Color(0, 0, 0));
        btnViewFeedback.setText("View Own Feedback");
        btnViewFeedback.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewFeedbackActionPerformed(evt);
            }
        });

        btnPassword.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnPassword.setText("Change Password");
        btnPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPasswordActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addComponent(lblWelcome))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(99, 99, 99)
                        .addComponent(btnLogOut))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnRequestMed)
                            .addComponent(btnViewRequest)
                            .addComponent(btnViewAppointment)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(btnPassword, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnViewFeedback, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnViewHistory, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(16, 16, 16))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(7, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(lblWelcome)
                .addGap(108, 108, 108)
                .addComponent(btnRequestMed)
                .addGap(18, 18, 18)
                .addComponent(btnViewRequest)
                .addGap(27, 27, 27)
                .addComponent(btnViewAppointment)
                .addGap(18, 18, 18)
                .addComponent(btnViewHistory)
                .addGap(18, 18, 18)
                .addComponent(btnViewFeedback, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnLogOut)
                .addGap(78, 78, 78))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogOutActionPerformed
        this.singleton.saveAll();
        toHomePage();
    }//GEN-LAST:event_btnLogOutActionPerformed

    private void btnViewRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewRequestActionPerformed
        toViewRequest();
    }//GEN-LAST:event_btnViewRequestActionPerformed

    private void btnViewAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewAppointmentActionPerformed
        ViewAppointments page = new ViewAppointments(this.theDoctor);
        page.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnViewAppointmentActionPerformed

    private void btnViewFeedbackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewFeedbackActionPerformed
        toViewFeedback();
    }//GEN-LAST:event_btnViewFeedbackActionPerformed

    private void btnViewHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewHistoryActionPerformed
        ViewPatients page = new ViewPatients(this.theDoctor);
        page.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnViewHistoryActionPerformed

    private void btnRequestMedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRequestMedActionPerformed
        controller.toRequestMedicine(this.theDoctor);
    }//GEN-LAST:event_btnRequestMedActionPerformed

    private void btnRemoveNotifActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveNotifActionPerformed
        int index = lstNotification.getSelectedIndex();
        
        if(index != -1)
        {
            notificationController.RemoveNotification(this.theDoctor, index);
            getNotifications();
        }
    }//GEN-LAST:event_btnRemoveNotifActionPerformed

    private void btnPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPasswordActionPerformed
        DoctorChangePassword page = new DoctorChangePassword(theDoctor);
        page.setVisible(true);
    }//GEN-LAST:event_btnPasswordActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DoctorHomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DoctorHomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DoctorHomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DoctorHomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogOut;
    private javax.swing.JButton btnPassword;
    private javax.swing.JButton btnRemoveNotif;
    private javax.swing.JButton btnRequestMed;
    private javax.swing.JButton btnViewAppointment;
    private javax.swing.JButton btnViewFeedback;
    private javax.swing.JButton btnViewHistory;
    private javax.swing.JButton btnViewRequest;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblNotif;
    private javax.swing.JLabel lblWelcome;
    private javax.swing.JList<String> lstNotification;
    // End of variables declaration//GEN-END:variables
}
