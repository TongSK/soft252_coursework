/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiDoctor;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.doctorModel.Prescription;
import patientmanagementsystem.guiDoctor.DoctorController.AppointmentController;
import patientmanagementsystem.guiDoctor.DoctorController.ConsultationController;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.model.User;

/**
 *
 * @author sheke
 */
public class Consultation extends javax.swing.JFrame {
    
    private Singleton singleton = Singleton.getInstance();
    private AppointmentController controller = new AppointmentController();
    private ConsultationController consultationController = new ConsultationController();
    private ArrayList<User> allPatients = singleton.getPatients();
    private Doctor doctor;
    private Appointment appointment;
    private ArrayList<Prescription> appointmentPrescription;

    /**
     * Creates new form Consultation
     */
    public Consultation(Doctor theDoctor, Appointment theAppointment) {
        this.doctor = theDoctor;
        this.appointment = theAppointment;
        this.appointmentPrescription = this.appointment.getPrescribedMedicine();
        initComponents();
        outputAppointment();
        outputPrescription();
        radNotNeed.setSelected(true);
    }
    
    public void outputAppointment()
    {
        txtPatientName.setText(appointment.getPatient().getFirstName() + " " + appointment.getPatient().getLastName());
        txtPatientStreetName.setText(appointment.getPatient().getAddress());
        txtPatientPostCode.setText(appointment.getPatient().getPostCode());
        txtPatientSex.setText(appointment.getPatient().getGender());
        
        LocalDate now = LocalDate.now();
        LocalDate birthdate = appointment.getPatient().getDateOfBirth();
        Period period = Period.between(birthdate, now); //Calculate duration between now and birth date
        
        txtPatientAge.setText(String.valueOf(period.getYears()));
        
        txtDocName.setText(appointment.getDoctor().getFirstName() + " " + appointment.getDoctor().getLastName());
        txtDocStreetName.setText(appointment.getDoctor().getAddress());
        txtDocPostCode.setText(appointment.getDoctor().getPostCode());
    }
    
    public void outputPrescription()
    {
        DefaultListModel model = new DefaultListModel(); //Create a model to add values to
        model.clear();
        lstPrescription.setModel(model);
        
        if(this.appointmentPrescription.size() > 0)
        {
            for (int i = 0; i < this.appointmentPrescription.size(); i++) {
                Prescription prescribedMed = this.appointmentPrescription.get(i);
                String prescriptionInfo = prescribedMed.getMedicine().getName() + " QUANTITY: "+ String.valueOf(prescribedMed.getQuantity())+ " DOSAGE: "+ prescribedMed.getDosage() ;
                model.addElement(prescriptionInfo);
                lstPrescription.setModel(model); //Pass model of all names to the JList component so it is visible
            }
        }
        
    }
    
    private void getInput()
    {
        String notes = txaNotes.getText();
        
        if(notes.equals("") == false)
        {
            if(radNeed.isSelected() == true)
            {
                this.consultationController.finishConsultation(this.appointment, notes, this.doctor, this);
            }
            else if(radNotNeed.isSelected() == true) //If prescription not needed
            {
                this.appointmentPrescription = null;
                this.consultationController.finishConsultation(this.appointment, notes, this.doctor, this);
            }
            
            JOptionPane.showMessageDialog(this, "APPOINTMENT FINISHED");
        }
        else
        {
            JOptionPane.showMessageDialog(this, "NOTES MUST BE FILLED IN");
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGrpPres = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        panPatient = new javax.swing.JPanel();
        lblPatient = new javax.swing.JLabel();
        lblPName = new javax.swing.JLabel();
        lblStreetNameP = new javax.swing.JLabel();
        lblPpostCode = new javax.swing.JLabel();
        lblSexP = new javax.swing.JLabel();
        lblAgeP = new javax.swing.JLabel();
        txtPatientName = new javax.swing.JTextField();
        txtPatientStreetName = new javax.swing.JTextField();
        txtPatientPostCode = new javax.swing.JTextField();
        txtPatientSex = new javax.swing.JTextField();
        txtPatientAge = new javax.swing.JTextField();
        panDoctor = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        lblNameD = new javax.swing.JLabel();
        lblStreetNameD = new javax.swing.JLabel();
        lblPostCodeD = new javax.swing.JLabel();
        txtDocName = new javax.swing.JTextField();
        txtDocStreetName = new javax.swing.JTextField();
        txtDocPostCode = new javax.swing.JTextField();
        panNotes = new javax.swing.JPanel();
        lblNotes = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txaNotes = new javax.swing.JTextArea();
        panPres = new javax.swing.JPanel();
        radNeed = new javax.swing.JRadioButton();
        radNotNeed = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstPrescription = new javax.swing.JList<>();
        btnAdd = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        btnFinish = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Consultation", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 24))); // NOI18N

        panPatient.setBackground(new java.awt.Color(255, 255, 204));

        lblPatient.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        lblPatient.setForeground(new java.awt.Color(0, 0, 0));
        lblPatient.setText("Patient");

        lblPName.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblPName.setForeground(new java.awt.Color(0, 0, 0));
        lblPName.setText("Name");

        lblStreetNameP.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblStreetNameP.setForeground(new java.awt.Color(0, 0, 0));
        lblStreetNameP.setText("Street Name");

        lblPpostCode.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblPpostCode.setForeground(new java.awt.Color(0, 0, 0));
        lblPpostCode.setText("Post Code");

        lblSexP.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblSexP.setForeground(new java.awt.Color(0, 0, 0));
        lblSexP.setText("Sex");

        lblAgeP.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblAgeP.setForeground(new java.awt.Color(0, 0, 0));
        lblAgeP.setText("Age");

        txtPatientName.setEditable(false);
        txtPatientName.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        txtPatientStreetName.setEditable(false);
        txtPatientStreetName.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        txtPatientPostCode.setEditable(false);
        txtPatientPostCode.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        txtPatientSex.setEditable(false);
        txtPatientSex.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        txtPatientAge.setEditable(false);
        txtPatientAge.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        javax.swing.GroupLayout panPatientLayout = new javax.swing.GroupLayout(panPatient);
        panPatient.setLayout(panPatientLayout);
        panPatientLayout.setHorizontalGroup(
            panPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panPatientLayout.createSequentialGroup()
                .addGroup(panPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panPatientLayout.createSequentialGroup()
                        .addContainerGap(13, Short.MAX_VALUE)
                        .addGroup(panPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPpostCode, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblStreetNameP, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblSexP, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblAgeP, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblPName, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(panPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPatientName, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtPatientStreetName, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                                .addComponent(txtPatientPostCode, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtPatientAge, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtPatientSex, javax.swing.GroupLayout.Alignment.TRAILING))))
                    .addGroup(panPatientLayout.createSequentialGroup()
                        .addGap(155, 155, 155)
                        .addComponent(lblPatient)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(82, 82, 82))
        );
        panPatientLayout.setVerticalGroup(
            panPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panPatientLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(lblPatient)
                .addGap(24, 24, 24)
                .addGroup(panPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPName)
                    .addComponent(txtPatientName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(panPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPatientStreetName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblStreetNameP))
                .addGap(24, 24, 24)
                .addGroup(panPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPatientPostCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPpostCode))
                .addGap(24, 24, 24)
                .addGroup(panPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPatientSex, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSexP))
                .addGap(24, 24, 24)
                .addGroup(panPatientLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPatientAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAgeP))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        panDoctor.setBackground(new java.awt.Color(255, 255, 204));

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Doctor");

        lblNameD.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblNameD.setForeground(new java.awt.Color(0, 0, 0));
        lblNameD.setText("Name");

        lblStreetNameD.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblStreetNameD.setForeground(new java.awt.Color(0, 0, 0));
        lblStreetNameD.setText("Street Name");

        lblPostCodeD.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        lblPostCodeD.setForeground(new java.awt.Color(0, 0, 0));
        lblPostCodeD.setText("Post Code");

        txtDocName.setEditable(false);
        txtDocName.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        txtDocStreetName.setEditable(false);
        txtDocStreetName.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        txtDocPostCode.setEditable(false);
        txtDocPostCode.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        javax.swing.GroupLayout panDoctorLayout = new javax.swing.GroupLayout(panDoctor);
        panDoctor.setLayout(panDoctorLayout);
        panDoctorLayout.setHorizontalGroup(
            panDoctorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panDoctorLayout.createSequentialGroup()
                .addGroup(panDoctorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panDoctorLayout.createSequentialGroup()
                        .addGap(142, 142, 142)
                        .addComponent(jLabel6))
                    .addGroup(panDoctorLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(panDoctorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblStreetNameD)
                            .addComponent(lblNameD)
                            .addComponent(lblPostCodeD))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panDoctorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDocName, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDocStreetName, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDocPostCode, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        panDoctorLayout.setVerticalGroup(
            panDoctorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panDoctorLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(panDoctorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblPostCodeD)
                    .addGroup(panDoctorLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(25, 25, 25)
                        .addGroup(panDoctorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblNameD)
                            .addComponent(txtDocName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(25, 25, 25)
                        .addGroup(panDoctorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDocStreetName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblStreetNameD))
                        .addGap(25, 25, 25)
                        .addComponent(txtDocPostCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panNotes.setBackground(new java.awt.Color(255, 255, 204));

        lblNotes.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        lblNotes.setForeground(new java.awt.Color(0, 0, 0));
        lblNotes.setText("Notes");

        txaNotes.setColumns(20);
        txaNotes.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        txaNotes.setRows(5);
        jScrollPane1.setViewportView(txaNotes);

        javax.swing.GroupLayout panNotesLayout = new javax.swing.GroupLayout(panNotes);
        panNotes.setLayout(panNotesLayout);
        panNotesLayout.setHorizontalGroup(
            panNotesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panNotesLayout.createSequentialGroup()
                .addGroup(panNotesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panNotesLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 586, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panNotesLayout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(lblNotes)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panNotesLayout.setVerticalGroup(
            panNotesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panNotesLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(lblNotes)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
                .addContainerGap())
        );

        panPres.setBackground(new java.awt.Color(255, 255, 204));
        panPres.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Prescription", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 18))); // NOI18N

        btnGrpPres.add(radNeed);
        radNeed.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        radNeed.setText("Patient Needs Prescription");

        btnGrpPres.add(radNotNeed);
        radNotNeed.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        radNotNeed.setText("Patient Does Not Need Prescription");

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        lstPrescription.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jScrollPane2.setViewportView(lstPrescription);

        btnAdd.setBackground(new java.awt.Color(255, 255, 204));
        btnAdd.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnAdd.setForeground(new java.awt.Color(0, 0, 0));
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnRemove.setBackground(new java.awt.Color(255, 255, 204));
        btnRemove.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnRemove.setForeground(new java.awt.Color(0, 0, 0));
        btnRemove.setText("Remove");
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(208, 208, 208)
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57)
                        .addComponent(btnRemove, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 608, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(17, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnRemove))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panPresLayout = new javax.swing.GroupLayout(panPres);
        panPres.setLayout(panPresLayout);
        panPresLayout.setHorizontalGroup(
            panPresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panPresLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panPresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(radNotNeed, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(radNeed, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(209, 209, 209))
            .addGroup(panPresLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panPresLayout.setVerticalGroup(
            panPresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panPresLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(radNeed)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radNotNeed)
                .addGap(29, 29, 29)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnFinish.setBackground(new java.awt.Color(255, 255, 204));
        btnFinish.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnFinish.setForeground(new java.awt.Color(0, 0, 0));
        btnFinish.setText("Finish Consultation");
        btnFinish.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinishActionPerformed(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(255, 255, 204));
        btnBack.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnBack.setForeground(new java.awt.Color(0, 0, 0));
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panNotes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(panPatient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panDoctor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(panPres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addComponent(btnFinish)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(85, 85, 85))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(panPres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnFinish)
                            .addComponent(btnBack)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panDoctor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panPatient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(27, 27, 27)
                        .addComponent(panNotes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(9, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
      this.consultationController.toPrescriptionPage(this, this.doctor, this.appointment);
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        int selected = lstPrescription.getSelectedIndex();
        if(selected != -1)
        {
            this.appointment = this.consultationController.removePrescription(this.appointment, selected);
            outputPrescription(); //Refresh page to see changes
        }
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        DoctorHomePage home = new DoctorHomePage(this.doctor);
        home.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnFinishActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinishActionPerformed
        getInput();
    }//GEN-LAST:event_btnFinishActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Consultation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Consultation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Consultation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Consultation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new Consultation().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnFinish;
    private javax.swing.ButtonGroup btnGrpPres;
    private javax.swing.JButton btnRemove;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblAgeP;
    private javax.swing.JLabel lblNameD;
    private javax.swing.JLabel lblNotes;
    private javax.swing.JLabel lblPName;
    private javax.swing.JLabel lblPatient;
    private javax.swing.JLabel lblPostCodeD;
    private javax.swing.JLabel lblPpostCode;
    private javax.swing.JLabel lblSexP;
    private javax.swing.JLabel lblStreetNameD;
    private javax.swing.JLabel lblStreetNameP;
    private javax.swing.JList<String> lstPrescription;
    private javax.swing.JPanel panDoctor;
    private javax.swing.JPanel panNotes;
    private javax.swing.JPanel panPatient;
    private javax.swing.JPanel panPres;
    private javax.swing.JRadioButton radNeed;
    private javax.swing.JRadioButton radNotNeed;
    private javax.swing.JTextArea txaNotes;
    private javax.swing.JTextField txtDocName;
    private javax.swing.JTextField txtDocPostCode;
    private javax.swing.JTextField txtDocStreetName;
    private javax.swing.JTextField txtPatientAge;
    private javax.swing.JTextField txtPatientName;
    private javax.swing.JTextField txtPatientPostCode;
    private javax.swing.JTextField txtPatientSex;
    private javax.swing.JTextField txtPatientStreetName;
    // End of variables declaration//GEN-END:variables
}
