/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiPatient.PatientController;

import java.util.ArrayList;
import patientmanagementsystem.Controller.GetUserController;
import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.model.User;
import patientmanagementsystem.patientModel.AccountRemovalRequest;
import patientmanagementsystem.patientModel.AppointmentRequest;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author shekei
 */
public class RemoveAccountController {
    
    private GetUserController controller = new GetUserController();
    private Singleton singleton = Singleton.getInstance();
    
    public RemoveAccountController()
    {
    
    }
    
    //Create an accountRemovalRequest object to send to secretary
    public void sendRemovalRequest(Patient sender)
    {
        Secretary secretary = controller.getRandomSecretary();
        secretary = sender.createRemovalAccountRequest(secretary);
        singleton.saveUser(secretary); //Save secretary after receiving new request
    }
    
    //Get arrayList of only removalRequests
    public ArrayList<Request> getRemovalRequests(Secretary secretary)
    {
        ArrayList<Request> allRequests = secretary.getAllRequests();
        ArrayList<Request> allRemovalRequests = filterRequests(allRequests);
        
        return allRemovalRequests;
    }
    
    public ArrayList<Request> filterRequests(ArrayList<Request> currentRequests)
    {
        ArrayList<Request> removalRequests = new ArrayList<>();
        boolean existance = false;
        
        for (int i = 0; i < currentRequests.size(); i++) {
            
            Request thisRequest = currentRequests.get(i); //For each request in request arrayList
            
            if(thisRequest.getRequestType().equals("REMOVE REQUEST")==true); //If request is a removalRequest subclass
            {
                AccountRemovalRequest request = (AccountRemovalRequest) thisRequest;
                existance = checkIfPatientExists(request);
                
                if(existance == true) //If patient has not already been deleted
                {
                    removalRequests.add(thisRequest);
                    existance = false; //reset to original value
                }
                
            }
            
        }
        
        return removalRequests;
    }
    
    public boolean checkIfPatientExists(AccountRemovalRequest checkRequest)
    {
        ArrayList<User> allPatients = singleton.getPatients();
        Patient patient;
        boolean patientExists = false;
        
        for (int i = 0; i < allPatients.size(); i++) {
            
            patient = (Patient)allPatients.get(i);
            
            if(patient.getUserId().equals(checkRequest.getPatient().getUserId())==true) //If ids match, patient exists
            {
                patientExists = true;
                break;
            }
        }
        
        return patientExists;
    }
    
    public void removePatientAccount(Patient patient, Secretary secretary)
    {
        ArrayList<User> allPatients = singleton.getPatients();
        
        for (int i = 0; i < allPatients.size(); i++) {
            
            Patient patientInArray = (Patient) allPatients.get(i);
            
            if(patientInArray.getUserId().equals(patient.getUserId())==true) //If patient found in arrayList
            {
                allPatients.remove(i);
                singleton.setPatients(allPatients); //Set arrayList back into singleton
                singleton.saveAll(); //Save change
                secretary = cleanUpSecretaryRequests(secretary, patient);
                singleton.saveUser(secretary); //Save changes;
                break;
            }
            
        }
    }
    
    //Remove any requests that were made from removed patient
    public Secretary cleanUpSecretaryRequests(Secretary secretary, Patient removedPatient)
    {
        ArrayList<Request> allRequests = secretary.getAllRequests();
        
        for (int i = 0; i < allRequests.size(); i++) {
            
            Request theRequest = allRequests.get(i);
            
            if(theRequest.getRequestType().equals("APPOINTMENT REQUEST")==true ) //If object is appointmentRequest subclass
            {
                AppointmentRequest appointmentRequest = (AppointmentRequest) theRequest;
                
                if(appointmentRequest.getPatient().getUserId().equals(removedPatient.getUserId()) == true)
                {
                    allRequests.remove(i);
                }
            }
            else if(theRequest.getRequestType().equals("REMOVE REQUEST")==true) //If object is removeRequest subclass
            {
                AccountRemovalRequest removeRequest = (AccountRemovalRequest) theRequest;
                
                if(removeRequest.getPatient().getUserId().equals(removedPatient.getUserId()) == true)
                {
                    allRequests.remove(i);
                }
            }
            
        }
        
        secretary.setAllRequests(allRequests); //Set changed arrayList back into secretary
        
        return secretary;
    }
    
    
}
