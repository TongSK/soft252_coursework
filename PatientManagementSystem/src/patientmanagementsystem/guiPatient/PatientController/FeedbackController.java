/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiPatient.PatientController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;
import patientmanagementsystem.Controller.GetUserController;
import patientmanagementsystem.adminModel.Admin;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.guiPatient.FeedbackPage;
import patientmanagementsystem.guiPatient.ViewAppointment;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.model.User;
import patientmanagementsystem.patientModel.FeedbackRequest;
import patientmanagementsystem.patientModel.Patient;

/**
 *
 * @author sheke
 */
public class FeedbackController {
    
    private Singleton singleton = Singleton.getInstance();
    private GetUserController controller = new GetUserController();
    
    public FeedbackController()
    {
    
    }
   
    public void toViewAppointmentPage(FeedbackPage currentPage, Appointment appointment, Patient patient)
    {
        ViewAppointment back = new ViewAppointment(patient, appointment);
        back.setVisible(true);
        currentPage.dispose();
    }
    
    //Using IState, create feedback request if appointment has IState of awaiting, else return null object
    public boolean checkAllowFeedback(Doctor doctor, String rating, String comment, Appointment appointment, Patient sender)
    {
        FeedbackRequest theRequest = appointment.allowFeedback(doctor, rating, comment);
        boolean nullRequest = true;
        
        if(theRequest != null)
        {
            nullRequest = false;
            Admin theAdmin = controller.getRandomAdmin();
            String notification = LocalDate.now() + " FEEDBACK REQUEST SENT";
            sender.sendRequest(theRequest, theAdmin, sender, notification);
            singleton.saveUser(theAdmin);
        }
        
        return nullRequest;
    }
   
    public void toFeedbackPage(ViewAppointment thisPage, Appointment appointment, Patient patient)
    {
        FeedbackPage page = new FeedbackPage( patient, appointment);
        page.setVisible(true);
        thisPage.dispose();
    }
    
}
