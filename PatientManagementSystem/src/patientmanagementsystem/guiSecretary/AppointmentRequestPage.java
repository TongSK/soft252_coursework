/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiSecretary;

import java.time.LocalDate;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import patientmanagementsystem.Controller.AppointmentRequestController;
import patientmanagementsystem.Controller.AppointmentRequestsController;
import patientmanagementsystem.doctorModel.Doctor;
import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.patientModel.AppointmentRequest;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.secretaryModel.Secretary;


/**
 *
 * @author shekei
 */
public class AppointmentRequestPage extends javax.swing.JFrame {
    
    private Secretary secretary;
    AppointmentRequestController appointmentCreator = new AppointmentRequestController();
    private AppointmentRequestsController filterController = new AppointmentRequestsController();
    private ArrayList<Request> allRequests;
    private ArrayList<Request> filteredRequests;
    private Singleton singleton = Singleton.getInstance();

    /**
     * Creates new form AppointmentRequest
     */
    public AppointmentRequestPage(Secretary theSecretary) {
        this.secretary = theSecretary;
        this.allRequests = secretary.getAllRequests(); //Get ArrayList of RequestObjects
        initComponents();
        btnSend.setVisible(false);
        btnSendPatient.setVisible(false);
        btnCreateAppointment.setVisible(false);
    }
    
      public void viewRequests()
    {
        String status = "";
        ArrayList<Request> selectedRequests = new ArrayList();
        
        if(radInitial.isSelected() == true)
        {
            status = "PATIENT REQUEST";
        }
        else if(radDocProposal.isSelected() == true)
        {
            status = "DOCTOR PROPOSAL";
        }
        else if(radPatientResponse.isSelected())
        {
            status = "PATIENT ACCEPTED";
        }
        
        if(status.equals("")== false)
        {
            this.filteredRequests = filterController.filterAppointmentRequests(status, allRequests);
            outputRequests(this.filteredRequests);
        }
    }
      
    public void outputRequests(ArrayList<Request> requests)
    {
        DefaultListModel model = new DefaultListModel(); //Create a model to add values to
        model.clear();
        lstRequest.setModel(model);
        
        if(requests != null ) 
        {
            for (int i = 0; i < requests.size(); i++) {
                
                AppointmentRequest appointment = (AppointmentRequest) requests.get(i);
                String appointmentName = appointment.getRequestStatus()+" "+appointment.getDoctor().getUserId() + " Dr. " + appointment.getDoctor().getLastName() + " with " + appointment.getPatient().getUserId();
                model.addElement(appointmentName);
                lstRequest.setModel(model); //Pass model of all names to the JList component so it is visible
            }
        }
    }
    
    public void removeRequest()
    {
        int output = lstRequest.getSelectedIndex(); //Get index from arrayList
        this.secretary = filterController.removeRequest(this.filteredRequests,output, this.secretary); //Remove and save changes
        singleton.saveUser(secretary);
        outputRequests(this.filteredRequests); //Refresh to see changes
    }
    
    public void sendRequestToDoctor()
    {
        int index = lstRequest.getSelectedIndex();
        
        if(index != -1) //If item has been selected
        {
            AppointmentRequest appointment = (AppointmentRequest) this.filteredRequests.get(index); //Get the appointment involved
            Doctor doctor = appointment.getDoctor(); //Get the doctor of the appointment
            String notification = LocalDate.now() + " - " + this.secretary.getUserId() + " sent APPOINTMENT REQUEST";
            this.secretary.sendRequest(appointment, doctor, this.secretary, notification);
            singleton.saveUser(doctor);
            JOptionPane.showMessageDialog(this ,"REQUEST SENT");
            
        }
        else
        {
            JOptionPane.showMessageDialog(this, "SELECTED NOTHING");
        }
    }
    
    public void sendRequestToPatient()
    {
        int index =lstRequest.getSelectedIndex();
        
        if(index != -1)
        {
            AppointmentRequest request = (AppointmentRequest)this.filteredRequests.get(index);
            request.setRequestStatus("SENT BY ADMIN");
            Patient receiver = request.getPatient();
            String notification = LocalDate.now() +" - "+ this.secretary.getUserId() + " - has sent DOCTOR APPOINTMENT REQUEST RESPONSE";
            this.secretary.sendRequest(request, receiver, this.secretary, notification);
            singleton.saveUser(receiver); //Save request to patient's account
            JOptionPane.showMessageDialog(this, "REQUEST SENT");
            removeRequest();
        }
        
        
    }
    
    public void createAppointment()
    {
        int index = lstRequest.getSelectedIndex();
        
        if(index != -1)
        {
            AppointmentRequest request = (AppointmentRequest)this.filteredRequests.get(index);
            this.appointmentCreator.turnRequestToAppointment(request);
            JOptionPane.showMessageDialog(this, "APPOINTMENT CREATED");
            removeRequest();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupStatus = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        panRequests = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstRequest = new javax.swing.JList<>();
        btnSend = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        btnCreateAppointment = new javax.swing.JButton();
        radInitial = new javax.swing.JRadioButton();
        radDocProposal = new javax.swing.JRadioButton();
        radPatientResponse = new javax.swing.JRadioButton();
        btnRemove = new javax.swing.JButton();
        btnSendPatient = new javax.swing.JButton();
        btnSelect = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Appointment Requests", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 24))); // NOI18N

        panRequests.setBackground(new java.awt.Color(255, 255, 204));

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Appointment Requests:");

        lstRequest.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jScrollPane1.setViewportView(lstRequest);

        javax.swing.GroupLayout panRequestsLayout = new javax.swing.GroupLayout(panRequests);
        panRequests.setLayout(panRequestsLayout);
        panRequestsLayout.setHorizontalGroup(
            panRequestsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panRequestsLayout.createSequentialGroup()
                .addGroup(panRequestsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panRequestsLayout.createSequentialGroup()
                        .addGap(317, 317, 317)
                        .addComponent(jLabel1))
                    .addGroup(panRequestsLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 889, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(30, Short.MAX_VALUE))
        );
        panRequestsLayout.setVerticalGroup(
            panRequestsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panRequestsLayout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnSend.setBackground(new java.awt.Color(255, 255, 204));
        btnSend.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnSend.setForeground(new java.awt.Color(0, 0, 0));
        btnSend.setText("Send Request To Doctor");
        btnSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendActionPerformed(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(255, 255, 204));
        btnBack.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        btnBack.setForeground(new java.awt.Color(0, 0, 0));
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnCreateAppointment.setBackground(new java.awt.Color(255, 255, 204));
        btnCreateAppointment.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnCreateAppointment.setForeground(new java.awt.Color(0, 0, 0));
        btnCreateAppointment.setText("Create Appointment");
        btnCreateAppointment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateAppointmentActionPerformed(evt);
            }
        });

        btnGroupStatus.add(radInitial);
        radInitial.setText("Patient Initial Request");
        radInitial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                radInitialMouseClicked(evt);
            }
        });

        btnGroupStatus.add(radDocProposal);
        radDocProposal.setText("Doctor Proposal Time");
        radDocProposal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                radDocProposalMouseClicked(evt);
            }
        });

        btnGroupStatus.add(radPatientResponse);
        radPatientResponse.setText("Patient Proposal Response");
        radPatientResponse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                radPatientResponseMouseClicked(evt);
            }
        });

        btnRemove.setBackground(new java.awt.Color(255, 255, 204));
        btnRemove.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        btnRemove.setForeground(new java.awt.Color(0, 0, 0));
        btnRemove.setText("Remove Request");
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });

        btnSendPatient.setBackground(new java.awt.Color(255, 255, 204));
        btnSendPatient.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnSendPatient.setForeground(new java.awt.Color(0, 0, 0));
        btnSendPatient.setText("Send Doctor Proposal To Patient");
        btnSendPatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendPatientActionPerformed(evt);
            }
        });

        btnSelect.setText("Select");
        btnSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panRequests, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnRemove, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnSend, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnSendPatient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnCreateAppointment, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(218, 218, 218))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(radPatientResponse))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(radInitial)
                                    .addComponent(radDocProposal))
                                .addGap(18, 18, 18)
                                .addComponent(btnSelect))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(36, 36, 36)
                                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panRequests, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(112, 112, 112)
                .addComponent(btnSend, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSendPatient, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(btnCreateAppointment, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnRemove, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(radInitial)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radDocProposal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnSelect)
                        .addGap(25, 25, 25)))
                .addComponent(radPatientResponse)
                .addGap(18, 18, 18)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1248, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(203, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectActionPerformed
        viewRequests();
    }//GEN-LAST:event_btnSelectActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.singleton.saveUser(secretary);
        this.singleton.saveAll();
        SecretaryHomePage homePage = new SecretaryHomePage(this.secretary);
        homePage.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
       removeRequest();
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendActionPerformed
        sendRequestToDoctor();
    }//GEN-LAST:event_btnSendActionPerformed

    private void btnSendPatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendPatientActionPerformed
        sendRequestToPatient();
    }//GEN-LAST:event_btnSendPatientActionPerformed

    private void btnCreateAppointmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateAppointmentActionPerformed
        createAppointment();
    }//GEN-LAST:event_btnCreateAppointmentActionPerformed

    private void radInitialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_radInitialMouseClicked
        btnSend.setVisible(true);
        btnCreateAppointment.setVisible(false);
        btnSendPatient.setVisible(false);
    }//GEN-LAST:event_radInitialMouseClicked

    private void radDocProposalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_radDocProposalMouseClicked
        btnSendPatient.setVisible(true);
        btnCreateAppointment.setVisible(false);
        btnSend.setVisible(false);
    }//GEN-LAST:event_radDocProposalMouseClicked

    private void radPatientResponseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_radPatientResponseMouseClicked
        btnCreateAppointment.setVisible(true);
        btnSend.setVisible(false);
        btnSendPatient.setVisible(false);
    }//GEN-LAST:event_radPatientResponseMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AppointmentRequestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AppointmentRequestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AppointmentRequestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AppointmentRequestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new AppointmentRequestPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCreateAppointment;
    private javax.swing.ButtonGroup btnGroupStatus;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnSelect;
    private javax.swing.JButton btnSend;
    private javax.swing.JButton btnSendPatient;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<String> lstRequest;
    private javax.swing.JPanel panRequests;
    private javax.swing.JRadioButton radDocProposal;
    private javax.swing.JRadioButton radInitial;
    private javax.swing.JRadioButton radPatientResponse;
    // End of variables declaration//GEN-END:variables
}
