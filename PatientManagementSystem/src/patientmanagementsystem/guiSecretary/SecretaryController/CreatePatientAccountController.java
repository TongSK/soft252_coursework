/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiSecretary.SecretaryController;

import java.time.LocalDate;
import java.util.ArrayList;
import patientmanagementsystem.Controller.CreateAccountController;
import patientmanagementsystem.CreateAccountRequest;
import patientmanagementsystem.guiAdmin.CreateAccount;
import patientmanagementsystem.model.AccountCreationRequest;
import patientmanagementsystem.model.Request;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.secretaryModel.Secretary;
import patientmanagementsystem.guiSecretary.CreateAccountPage;
import patientmanagementsystem.guiSecretary.CreateAccountRequestPage;

/**
 *
 * @author shekei
 */
public class CreatePatientAccountController {
    
    private CreateAccountController controller = new CreateAccountController();
    private Secretary theSecretary;
    private ArrayList<Request> allRequests;
    private ArrayList<AccountCreationRequest> accountRequests = new ArrayList<AccountCreationRequest>();
    private Singleton singleton = Singleton.getInstance();
    
    
    public CreatePatientAccountController(Secretary secretary)
    {
        this.theSecretary = secretary;
        this.allRequests = this.theSecretary.getAllRequests();
    }
    
    public ArrayList<AccountCreationRequest> getAccountRequests()
    {
        String status = "CREATE ACCOUNT REQUEST";
        
        for (int i = 0; i < this.allRequests.size(); i++) {
            
            Request request = this.allRequests.get(i);
            if(request.getRequestType().equals(status)) //Get all account requests
            {
                AccountCreationRequest theRequest = (AccountCreationRequest) request;
                this.accountRequests.add(theRequest); //add to arrayList
            }
            
        }
        
        return this.accountRequests;
        
    }
    
    //Create patient account using request object and user id and password input
    public void CreateAccount(String userId, String password, AccountCreationRequest theRequest)
    {
        this.controller.createPatientAccount(theRequest.getForename(), theRequest.getSurname(), userId, password, theRequest.getAddress(), theRequest.getPostCode(), theRequest.getGender(), theRequest.getDateOfBirth());
    }
    
    public void removeRequest(int requestIndex)
    {
        AccountCreationRequest removeRequest =(AccountCreationRequest) this.accountRequests.get(requestIndex);
        
        for (int i = 0; i < this.allRequests.size(); i++) {
            
            Request checkRequest = this.allRequests.get(i);
            
            if(checkRequest.getClass().equals(removeRequest.getClass()) == true)
            {
                AccountCreationRequest checkAccountRequest = (AccountCreationRequest) checkRequest;
                
                if(checkAccountRequest.getDateOfBirth().equals(removeRequest.getDateOfBirth()) == true && checkAccountRequest.getForename().equals(removeRequest.getForename()) == true && checkAccountRequest.getSurname().equals(removeRequest.getSurname()) == true )
                {
                    this.allRequests.remove(i);
                    this.theSecretary.setAllRequests(allRequests);
                    singleton.saveUser(theSecretary);
                }
            }
            
        }
    }
    
    public void toCreateAccountPage(Secretary secretary,CreatePatientAccountController theController, int theIndex, CreateAccountRequestPage thisPage)
    {
        CreateAccountPage nextPage = new CreateAccountPage(secretary, theController, theIndex);
        nextPage.setVisible(true);
        thisPage.dispose();
    }
    
    public void toRequestPage(Secretary secretary, CreateAccountPage thisPage )
    {
        CreateAccountRequestPage page = new CreateAccountRequestPage(secretary);
        page.setVisible(true);
        thisPage.dispose();
    }
    
}
