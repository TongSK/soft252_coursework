/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiSecretary.SecretaryController;

import java.util.ArrayList;
import patientmanagementsystem.doctorModel.Prescription;
import patientmanagementsystem.guiSecretary.GiveMedicine;
import patientmanagementsystem.guiSecretary.ViewPrescription;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.User;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author shekei
 */
public class ViewPrescriptionController {
    
    public ViewPrescriptionController()
    {
    
    }

    //Get list of appointments where patients have not received their prescription yet
    public ArrayList<Appointment> getAppointments(ArrayList<User> allPatients)
    {
        ArrayList<Appointment> relevantAppointments = new ArrayList<Appointment>();
        
        for (int i = 0; i < allPatients.size(); i++) {//For each patient get their ArrayList of appointments
            
            Patient patient =(Patient) allPatients.get(i);
            ArrayList<Appointment> allAppointments = patient.getAllAppointments();
            
            for (int j = 0; j < allAppointments.size(); j++) {//For each appointment, get the prescription status
                Appointment appointment = allAppointments.get(i);
                String status = appointment.getPrescriptionStatus();
                
                if(status.equals("NOT YET RECEIVED"))
                {
                        relevantAppointments.add(appointment);
                }
                
            }
            
        }
        
        return relevantAppointments;
        
    }
    
    public void toGiveMedicinePage(ViewPrescription thisPage, int appointmentIndex, Appointment appointment, Secretary secretary)
    {
        GiveMedicine page = new GiveMedicine(secretary, appointment, appointmentIndex);
        page.setVisible(true);
        thisPage.dispose();
    }
        
    
    
    }
