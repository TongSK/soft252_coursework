/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiSecretary.SecretaryController;

import java.util.ArrayList;
import patientmanagementsystem.doctorModel.Prescription;
import patientmanagementsystem.guiSecretary.GiveMedicine;
import patientmanagementsystem.guiSecretary.ViewPrescription;
import patientmanagementsystem.model.Appointment;
import patientmanagementsystem.model.Medicine;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.patientModel.Patient;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author shekei
 */
public class GiveMedicineController {
    
    Singleton singleton = Singleton.getInstance();
    
    public GiveMedicineController()
    {
        
    }
    
    //appointment index to show where appointment object is in arrayList
    public boolean giveMedicine(Appointment appointment, int appointmentIndex, int prescribedIndex, Secretary secretary)
    {
        boolean given = false;
        
        Patient patient = appointment.getPatient(); //Get patient involved in prescription
        Medicine prescribedMedicine = appointment.getPrescribedMedicine().get(prescribedIndex).getMedicine(); //Get medicine we are prescribing
        ArrayList<Medicine> allMedicine = singleton.getMedicine();
        Medicine theMedicine =null;
        
        for (int i = 0; i < allMedicine.size(); i++) {
            theMedicine = allMedicine.get(i);
            if(theMedicine.getName().equals(prescribedMedicine.getName())== true) //Get same medicine but from singleton 
            {
                patient = secretary.giveMedicine(theMedicine, appointment, prescribedIndex, appointmentIndex);
            }
        }
        
        singleton.saveUser(patient); //Save patient back to json
        
        checkIfPrescriptionFinished(appointment, appointmentIndex); //Check if this was last medicine to be prescribed and update appointment prescription status if so
        
        //Check if medicine was given to patient
        if(patient.getAllAppointments().get(appointmentIndex).getPrescribedMedicine().get(prescribedIndex).getReceived() == true)
        {
            given = true;
        }
        return given;
    }
    
    public void goBack(GiveMedicine thisPage, Secretary secretary)
    {
        ViewPrescription backPage = new ViewPrescription(secretary);
        backPage.setVisible(true);
        thisPage.dispose();
    }
    
    //Check if all prescribed med has been given and then change prescription status for appointment object as received
    public void checkIfPrescriptionFinished(Appointment appointment, int appointmentIndex)
    {
        boolean allReceived = true; 
        Patient thePatient = appointment.getPatient();
        ArrayList<Prescription> allPrescriptions = appointment.getPrescribedMedicine();
        
        for (int i = 0; i < allPrescriptions.size(); i++) {
            
            Prescription prescription = allPrescriptions.get(i);
            
            if(prescription.getReceived() == false)
            {
                allReceived = false;
                break;
            }
            
        }
        
        if(allReceived == true)
        {
            appointment.setPrescriptionStatus("RECEIVED");
            ArrayList<Appointment> allAppointments = thePatient.getAllAppointments();
            allAppointments.remove(appointmentIndex); //remove old object
            allAppointments.add(appointment); //Replace with new
            thePatient.setAllAppointments(allAppointments); //Save back to patient object
            singleton.saveUser(thePatient);
            
        }
    }
}
