/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem.guiSecretary.SecretaryController;

import java.util.ArrayList;
import patientmanagementsystem.guiSecretary.RestockMedicine;
import patientmanagementsystem.guiSecretary.SecretaryHomePage;
import patientmanagementsystem.guiSecretary.ViewMedicine;
import patientmanagementsystem.model.Medicine;
import patientmanagementsystem.model.Singleton;
import patientmanagementsystem.secretaryModel.Secretary;

/**
 *
 * @author shekei
 */
public class RestockController {
    Singleton singleton = Singleton.getInstance();
    
    public RestockController()
    {
    
    }
    
    public void toHomePage(Secretary secretary, ViewMedicine thisPage)
    {
       SecretaryHomePage home = new SecretaryHomePage(secretary);
       home.setVisible(true);
       thisPage.dispose();
    }
    
    public void toRestockPage(int index, Secretary secretary, ViewMedicine thisPage)
    {
        RestockMedicine restockPage = new RestockMedicine(secretary, index);
        restockPage.setVisible(true);
        thisPage.dispose();
    }
    
    //medIndex = where the medicine we need to restock is in the ArrayList
    public void restockMed(int medIndex, Secretary secretary, int stockAmount, RestockMedicine currentPage)
    {
        Medicine restockMedicine = singleton.getMedicine().get(medIndex);
        restockMedicine = secretary.updateMedStock(restockMedicine, stockAmount); //Update stock number
        ArrayList<Medicine> allMedicine = singleton.getMedicine();
        allMedicine.remove(medIndex);
        allMedicine.add(restockMedicine);
        singleton.setMedicine(allMedicine);
        singleton.saveAll(); //Save medicine arrayList back to json
        goBackToViewMedicine(currentPage, secretary);
    }
    
    public void goBackToViewMedicine(RestockMedicine thisPage, Secretary secretary)
    {
        ViewMedicine page = new ViewMedicine(secretary);
        page.setVisible(true);
        thisPage.dispose();
    }
    
}
